const staticCacheName = 'webmotors-2019-01-03-00-00';
const filesToCache = [
    '/offline/login.html',
    '/bundle.js',
    '/style.css',
];

// Cache on install
this.addEventListener('install', (event) => {
    this.skipWaiting();
    event.waitUntil(
        caches.open(staticCacheName)
            .then(cache => cache.addAll(filesToCache)),
    );
});

// Clear cache on activate
this.addEventListener('activate', (event) => {
    event.waitUntil(
        caches.keys().then(cacheNames => Promise.all(
            cacheNames
                .filter(cacheName => (cacheName.startsWith('webmotors-')))
                .filter(cacheName => (cacheName !== staticCacheName))
                .map(cacheName => caches.delete(cacheName)),
        )),
    );
});

// Serve from Cache
this.addEventListener('fetch', (event) => {
    event.respondWith(
        caches.match(event.request)
            .then(response => response || fetch(event.request))
            .catch(() => caches.match('/offline/login.html')),
    );
});
