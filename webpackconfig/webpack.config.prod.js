const path = require('path')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CopyPlugin = require('copy-webpack-plugin')

const entry = require('./config/entry')
const resolve = require('./config/resolve')
const target = require('./config/target')
const modules = require('./config/module')
const urls = require('./config/urls')
const scripts = require('./config/scripts')

const environment = process.env.NODE_ENV

const now = new Date()
const dateStr = `${now.getFullYear()}${now.getMonth() + 1}${now.getDate()}-${now.getHours()}${now.getMinutes()}`

process.noDeprecation = true

module.exports = {
  entry,
  resolve,
  target,
  module: modules,
  plugins: [
    new CleanWebpackPlugin(),
    new MiniCssExtractPlugin({
      filename: 'style.css?t=[contenthash]'
    }),
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: 'index.html',
      dateStr,
      environment,
      chunks: ['page'],
      urls,
      scripts
    }),
    new CopyPlugin([
      { from: 'public', to: '' }
    ])
  ],
  output: {
    path: path.join(process.cwd(), `/publish/${environment}/source/cockpit`),
    publicPath: '/',
    filename: 'bundle.js?t=[contenthash]'
  }
}
