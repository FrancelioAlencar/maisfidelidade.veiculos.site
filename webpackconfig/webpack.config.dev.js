const webpack = require('webpack')
const path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const jsonRequest = require('../aws/edge/lambdaViewer/test.json')
const lambdaViewer = require('../aws/edge/lambdaViewer/index')

const entry = require('./config/entry')
const resolve = require('./config/resolve')
const target = require('./config/target')
const modules = require('./config/module')
const urls = require('./config/urls')
const scripts = require('./config/scripts')

const environment = process.env.NODE_ENV

process.noDeprecation = true

module.exports = {
  entry,
  resolve,
  target,
  module: modules,
  devtool: 'inline-source-map',
  output: {
    path: path.join(process.cwd(), '.build'),
    publicPath: '/',
    filename: 'bundle.js?t=[hash]'
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: 'style.css?t=[contenthash]'
    }),
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: 'index.html',
      chunks: ['page'],
      environment,
      urls,
      scripts
    }),
    new webpack.NamedModulesPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.DefinePlugin({
      'process.env': { BUILD_TARGET: JSON.stringify('client') }
    })
  ],
  devServer: {
    openPage: 'http://local.webmotors.com.br:8000',
    contentBase: [path.join(__dirname, '../public')],
    compress: true,
    inline: true,
    host: 'localhost',
    port: 8000,
    historyApiFallback: true,
    disableHostCheck: true,
    proxy: {
      '*': {
        bypass: (req) => {
          jsonRequest.Records[0].cf.request.uri = req.originalUrl
          lambdaViewer.handler(jsonRequest, null, (err, res) => {
            req.originalUrl = res.uri
          })
          return req.originalUrl
        }
      }
    },
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
      'Access-Control-Allow-Headers': 'X-Requested-With, content-type, Authorization'
    }
  }
}
