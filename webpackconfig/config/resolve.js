const path = require('path')

module.exports = {
  extensions: ['.js', '.jsx'],
  alias: {
    react: path.resolve(__dirname, '../../node_modules/react'),
    'styled-components': path.resolve(__dirname, '../../node_modules/styled-components'),
    Components: path.resolve(__dirname, '../../client/html/Components/'),
    Containers: path.resolve(__dirname, '../../client/html/Containers/'),
    Api: path.resolve(__dirname, '../../client/js/Api/'),
    Helpers: path.resolve(__dirname, '../../client/js/Helpers/'),
    Libraries: path.resolve(__dirname, '../../client/js/Libraries/'),
    scss: path.resolve(__dirname, '../../client/scss/'),
    'webmotors-react-pj/config': path.resolve(__dirname, `../../../webmotors.react.pj/dist/config/env/${process.env.NODE_ENV}`),
    'webmotors-react-pj': path.resolve(__dirname, '../../../webmotors.react.pj/dist'),
    '@': path.resolve(__dirname, '../../src')
  }
}
