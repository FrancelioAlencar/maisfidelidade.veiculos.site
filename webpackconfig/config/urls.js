const config = {
    ApiLogin: {
        development: '//localhost/WebMotors.API/login',
        homologation: 'https://hcockpit.webmotors.com.br/api/login',
        blue: 'https://azulcockpit.webmotors.com.br/api/login',
        production: 'https://cockpit.webmotors.com.br/api/login',
    },
    ApiCockpit: {
        development: 'https://localhost:44351/api',
        homologation: 'https://hcockpit.webmotors.com.br/api',
        blue: 'https://azulcockpit.webmotors.com.br/api',
        production: 'https://cockpit.webmotors.com.br/api',
    },
    ApiSisense: {
        development: 'https://sisense.webmotors.com.br/api/',
        homologation: 'https://sisense.webmotors.com.br/api/',
        blue: 'https://sisense.webmotors.com.br/api/',
        production: 'https://sisense.webmotors.com.br/api/',
    },
    ApiStore: {
        development: 'https://localhost:44365/api/cockpit/shopping',
        homologation: 'https://hcockpit.webmotors.com.br/api/cockpit/shopping',
        blue: 'https://azulcockpit.webmotors.com.br/api/cockpit/shopping',
        production: 'https://cockpit.webmotors.com.br/api/cockpit/shopping',
    },
    ApiMaisFidelidade: {
        development: 'https://localhost/webmotors.com.br/cockpit.maisfidelidade',
        homologation: 'https://hcockpit.webmotors.com.br/api/cockpit/maisfidelidade',
        blue: 'https://azulcockpit.webmotors.com.br/api/cockpit/maisfidelidade',
        production: 'https://cockpit.webmotors.com.br/api/cockpit/maisfidelidade',
    },
    UrlCockpit: {
        development: 'http://local.webmotors.com.br:8000',
        homologation: 'https://hcockpit.webmotors.com.br',
        blue: 'https://azulcockpit.webmotors.com.br',
        production: 'https://cockpit.webmotors.com.br',
    },
    UrlCRM: {
        development: 'http://local.webmotors.com.br:81/plataformarevendedor',
        homologation: 'https://hkcrm.webmotors.com.br',
        blue: 'https://azulcrm.webmotors.com.br',
        production: 'https://crm.webmotors.com.br',
    },
    UrlEstoque: {
        development: 'http://local.webmotors.com.br:8080',
        homologation: 'https://hestoque.webmotors.com.br',
        blue: 'https://azulestoque.webmotors.com.br',
        production: 'https://estoque.webmotors.com.br',
    },
    UrlMaisFidelidade: {
        development: 'http://local.webmotors.com.br:8080',
        homologation: 'https://hcockpit.webmotors.com.br/maisfidelidade',
        blue: 'https://azulcockpit.webmotors.com.br/maisfidelidade',
        production: 'https://cockpit.webmotors.com.br/maisfidelidade',
    },
};

module.exports = config;
