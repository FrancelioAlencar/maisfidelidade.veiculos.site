# base on carbon nodejs version
FROM node:carbon as react-pj
MAINTAINER Infraestrutura <infra@webmotors.com.br>

# update npm
RUN npm i npm@latest -g
RUN npm install --save-dev @babel/core @babel/preset-env @babel/preset-stage-2 @babel/plugin-transform-runtime

# copy app folders react pj
RUN mkdir -p /usr/src/app
RUN mkdir -p /usr/src/app/webmotors.react.pj
COPY ./webmotors.react.pj /usr/src/app/webmotors.react.pj
RUN cd /usr/src/app/webmotors.react.pj
WORKDIR /usr/src/app/webmotors.react.pj

RUN npm i
RUN npm start

# copy folder maisfidelidade
RUN mkdir -p /usr/src/app/webmotors.maisfidelidade.new
COPY . /usr/src/app/webmotors.maisfidelidade.new
WORKDIR /usr/src/app/webmotors.maisfidelidade.new
RUN npm install --silent
RUN npm run hml --silent

# Stage 2 - the production environment
FROM nginx:alpine
COPY --from=react-pj /usr/src/app/webmotors.maisfidelidade.new/publish/homologation/source/cockpit /var/www
COPY --from=react-pj /usr/src/app/webmotors.maisfidelidade.new/healthcheck.html /var/www
COPY nginx.conf /etc/nginx/nginx.conf
EXPOSE 8080
ENTRYPOINT ["nginx","-g","daemon off;"]
