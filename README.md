Project | Mais Fidelidade - Autos - Site
======================================

This project was developed using some libraries that the job market and community in general use containing a good file structure and code organization.

- Create React App by [create-react-app](https://github.com/facebook/create-react-app)
- React by [react](https://pt-br.reactjs.org/)
- Redux for state management among others, by [redux](https://redux.js.org/)
- React Router Dom by [react-router-dom](https://reacttraining.com/react-router/web/guides/quick-start)
- Axios by [axios](https://github.com/axios/axios)
- Styled Components by [styled-components](https://www.styled-components.com/)
- Jest by [jest](https://jestjs.io/)
- Design System - Atomic Design by [atomic-design](https://bradfrost.com/blog/post/atomic-web-design/)
- Husky by [husky](https://github.com/typicode/husky)
- Prettier by [prettier](https://prettier.io/) 
- EsLint | Standard by [eslint](https://eslint.org/)

## Getting Started

```sh
# clone it
    git clone https://bitbucket.org/FrancelioAlencar/maisfidelidade.autos.site/src/master/
    cd maisfidelidade.autos.site

# Install dependencies
    npm i

# Start project development
    npm start

# If you want to start production
    npm build

# To run application unit tests
    npm test
```