const sizeVw = size => (size * 100) / 1920

export default sizeVw
