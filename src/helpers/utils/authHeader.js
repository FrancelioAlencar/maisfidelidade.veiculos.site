import { cookieGet } from 'webmotors-react-pj/utils'

const authHeader = () => {
  const token = (cookieGet('CockpitLogged') || cookieGet('AppLogged'))
  if (token) {
    return {
      Authorization: `Bearer ${token}`,
      Pragma: 'no-cache'
    }
  }
  return {}
}

export default authHeader
