export default {
  'how-it-works': {
    placeholder: 'Qual sua dúvida?',
    title: 'Como Funciona'
  },
  informations: {
    'title-about': 'Sobre o +Fidelidade',
    version: 'Versão 1.0',
    description: 'É um programa de fidelidade que visa conceder aos nossos clientes uma oferta de valor completa, com benefícios e benfeitorias, conforme nível de vinculação e relacionamento com diversos produtos do Grupo Santander Brasil e Webmotors.',
    contact: {
      title: 'Fale Conosco',
      company: 'Santander',
      phone: '(11) 2345-6789',
      mail: 'contato@contato.com.br'
    }
  }
}
