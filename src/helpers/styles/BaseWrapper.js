import styled from 'styled-components'
import colors from './colors'

// eslint-disable-next-line import/prefer-default-export
export const Wrapper = styled.div`
  width: 100%;
  overflow-x: hidden;
  background: ${colors.white};
  height: calc(100vh - 151px);
`
export const WrapperContent = styled.div`
  width: calc(100% - 70px);
  display: inline-block;
  padding: 40px;
  border-top-left-radius: 20px;
  background: #f3f5fb;
  height: auto;
`
