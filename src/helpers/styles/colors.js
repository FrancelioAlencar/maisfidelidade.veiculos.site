const colors = {
  primaryColor: '#f60535',
  primaryColorHeavy: '#7e8188',
  white: '#FFFFFF'
}

const hexToRgb = hex => {
  const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex)
  return !result ? null : `${parseInt(result[1], 16)},${parseInt(result[2], 16)},${parseInt(result[3], 16)}`
}

const colorKeys = Object.keys(colors)
colors.rgb = {}
colorKeys.forEach(name => (colors.rgb[name] = hexToRgb(colors[name])))

export default colors
