import React, { useState, useEffect, Fragment } from 'react'
import { Timer, Moment } from './styled'

const Clock = () => {
  const [date, setDate] = useState(new Date())
  const [momenteDate, setMomentDate] = useState('')

  useEffect(() => {
    const timerID = setInterval(() => setDate(new Date()), 1000)

    if (date.getHours() >= 0 && date.getHours() < 12) setMomentDate('Bom dia, ')
    else if (date.getHours() >= 12 && date.getHours() <= 18) setMomentDate('Boa tarde, ')
    else setMomentDate('Boa noite, ')

    return () => clearInterval(timerID)
  }, [])

  return (
    <Fragment>
      <Moment>{momenteDate}</Moment>
      <Timer>{date.toLocaleTimeString()}</Timer>
    </Fragment>
  )
}

export default Clock
