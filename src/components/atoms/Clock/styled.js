import styled from 'styled-components'

// eslint-disable-next-line import/prefer-default-export
export const Timer = styled.div`
  font-size: 22px;
  line-height: 1.33;
  color: #34303f;
  font-family: 'Open Sans', sans-serif;
  font-weight: bold;
  display: inline-block;
  vertical-align: middle;
`

export const Moment = styled.div`
  font-size: 21px;
  font-family: 'Open Sans', sans-serif;
  color: #34303f;
  line-height: 1.33;
  display: inline-block;
  vertical-align: middle;
`
