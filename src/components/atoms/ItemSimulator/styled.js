import styled from 'styled-components'
import { sizeVw } from '@/helpers/utils'

export const Item = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  width: ${({ big }) => big ? `98%` : '48%'};
  background-color: #fff;
  border-radius: 10px;
  padding: 0 ${sizeVw(40)}vw;
  margin: 10px 1%;
`

export const Header = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
  position: relative;
  border-bottom: ${({ open }) => open ? `1px` : '0'} solid #d9dadc;
  padding: 14px 0;

  &:after {
    position: absolute;
    content: "";
    top: calc(50% - 6px);
    right: -20.5px;
    width: 10px;
    height: 10px;
    border-style: solid;
    border-width: 0 1.5px 1.5px 0;
    border-color: #4d4c59;
    display: inline-block;
    padding: 4px;
    transform: ${({ open }) => open ? `rotate(-135deg)` : 'rotate(45deg)'};
    -webkit-transform: ${({ open }) => open ? `rotate(-135deg)` : 'rotate(45deg)'};
    cursor: 'pointer';
    z-index: 1;
  }
`

export const Description = styled.div`
  display: flex;
  padding-top: 14px;
  padding-bottom: 28px;
  flex: 1;
`

export const Information = styled.span`
  display: flex;
  justify-content: center;
  align-items: center;
  
  width: 16px;
  height: 16px;
  border-radius: 50%;
  background-color: #43bccd;
  margin-left: 5px;

  color: #FFF;
  font-family: Poppins;
  font-size: 12px;
`

export const DivProgress = styled.div`
  display: flex;
  width: 55%;
  height: 20px;
  flex-direction: column;
  align-items: flex-end;
  justify-content: center;
  margin-left: auto;

  .rc-slider-handle:hover:after{
    position: absolute;
    bottom: 150%;
    left: 50%;
    margin-bottom: 5px;
    margin-left: -36.35px;
    padding: 7px;
    width: 72.7px;
    -webkit-border-radius: 20px;
    -moz-border-radius: 20px;
    border-radius: 20px;
    background-color: #000;
    color: #fff;
    content: ${({ value }) => value ? `"${value} pts"` : `"0 pts"`};
    text-align: center;
    font-size: 13px;
    font-family: Futura;
    font-weight: bold;
  }

  .rc-slider-handle:hover:before {
    position: absolute;
    bottom: 150%;
    left: 50%;
    margin-left: -5px;
    width: 0;
    border-top: 5px solid #000;
    border-top: 5px solid hsla(0, 0%, 20%, 0.9);
    border-right: 5px solid transparent;
    border-left: 5px solid transparent;
    content: " ";
    font-size: 0;
    line-height: 0;
  }

  
`

export const ProgressBar = styled.input`
  -webkit-appearance: none;
  width: 100%;
  height: 20px;
  background: ${({ percentage }) => percentage ? `linear-gradient(to right, #00e2ba ${percentage}% , #e5e7ef ${percentage + 1}%)` : '#e5e7ef'};
  border-radius: 10px;
  outline: none;
  -webkit-transition: .2s;
  transition: opacity .2s;
  opacity: 1;

  

  &::-webkit-slider-thumb {
    -webkit-appearance: none;
    appearance: none;
    width: 24px;
    height: 24px;
    background-color: #71727f;
    border: 6px solid #FFFFFF;
    border-radius: 50%;
    box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);
    cursor: pointer;

    position: relative;

    &:hover > div#div-input {
      width: 50% !important;
    }
  }
`

export const TooltipInput = styled.span`
  display: none;
  position: absolute;
  top: -20px;
  width: 20px;
  height: 20px;
  background-color: black;
  content:" ";
`

export const Progress = styled.div`
  width: 100%;
  position: relative;
  &:after{
    position: absolute;
    content: "X";
    left: 30%;
    display: none;
  }
`

export const LabelSwitch = styled.label`
  position: relative;
  display: inline-block;
  width: 40px;
  height: 20px;
  margin-left: auto;

  input {
    opacity: 0;
    width: 0;
    height: 0;
  }

  .slider {
    position: absolute;
    cursor: pointer;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: rgba(0,0,0,0);
    -webkit-transition: .4s;
    transition: .4s;
    border: solid 2px #e9eaf2;
  }

  .slider:before {
    position: absolute;
    content: "";
    height: 16px;
    width: 16px;
    left: 1px;
    bottom: .5px;
    background-color: #747382;
    -webkit-transition: .4s;
    transition: .4s;
  }
  
  input:checked + .slider {
    background-image: linear-gradient(to bottom, #0ec2a0 -78%, #00e2ba);
  }
  
  input:focus + .slider {
    box-shadow: 0 0 1px #2196F3;
  }
  
  input:checked + .slider:before {
    -webkit-transform: translateX(18.5px);
    -ms-transform: translateX(18.5px);
    transform: translateX(18.5px);
  }
  
  /* Rounded sliders */
  .slider.round {
    border-radius: 34px;
  }
  
  .slider.round:before {
    border-radius: 50%;
  }
`

export const Percentage = styled.label`
  font-size: ${sizeVw(30)}vw;
  font-weight: bold;
  line-height: 1.37;
  letter-spacing: -0.6px;
  color: #696977;
  margin-left: ${sizeVw(20)}vw;
  text-align: right;
  width: ${sizeVw(75)}vw;

  &:after {
    content: "%";
    font-size: ${sizeVw(22)}vw;
    font-weight: bold;
    color: #696977;
    vertical-align: top;
    display: inline-block;
    margin-top: 5px;
  }
`
