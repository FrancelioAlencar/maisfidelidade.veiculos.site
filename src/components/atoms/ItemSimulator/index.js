import React, { useState, Fragment } from 'react'
import { Label } from '@/components/atoms'
import Slider from 'rc-slider'
import {
  Item,
  Header,
  Description,
  Information,
  DivProgress,
  Percentage,
  LabelSwitch
} from './styled'
import 'rc-slider/assets/index.css'

const ItemSimulator = ({ item, onChange }) => {
  const [open, setOpen] = useState(false)

  const openCollapse = event => {
    event.preventDefault()
    event.stopPropagation()
    setOpen(!open)
  }

  const changeSwitch = () => {
    const bool = item.idType === 2 && item.value
    if (bool) {
      onChange(bool ? item.maxValue : 0, item.id)
    }
    return bool
  }
  const [checked, setChecked] = useState(() => changeSwitch())

  const toggleSwitch = (event, id) => {
    event.preventDefault()
    event.stopPropagation()
    setChecked(!checked)
    onChange(checked ? 0 : item.maxValue, id)
  }

  const progressBar = () => (
    <Fragment>
      <DivProgress
        onClick={event => {
          event.preventDefault()
          event.stopPropagation()
        }}
        value={item.value}
      >
        <Slider
          value={item.value}
          max={item.maxValue}
          min={0}
          railStyle={{ backgroundColor: '#e5e7ef', height: 20, borderRadius: '10px' }}
          trackStyle={{ background: 'linear-gradient(to bottom, #0ec2a0 -78%, #00e2ba)', height: 20, borderRadius: '10px' }}
          onChange={num => onChange(num, item.id)}
          style={{ height: '20px', padding: 0 }}
          handleStyle={{
            height: 30,
            width: 30,
            backgroundColor: '#686876',
            border: '8px solid #ffffff',
            'box-shadow': '0 3px 6px 0 rgba(0, 0, 0, 0.16)'
          }}
        />
      </DivProgress>
      <Percentage>{parseInt(((item.value * 100) / item.maxValue) || 0)}</Percentage>
    </Fragment>
  )

  const switchBar = () => (
    <Fragment>
      <LabelSwitch onClick={(event) => toggleSwitch(event, item.id)}>
        <input type="checkbox" checked={checked} />
        <span className="slider round" />
      </LabelSwitch>
    </Fragment>
  )

  const renderInput = id => {
    if (id === 1) {
      return progressBar()
    }
    if (id === 2) {
      return switchBar()
    }
    return null
  }

  return (
    <Item big={item.idType === 1}>
      <Header
        idType={item.idType}
        open={open}
        onClick={(event) => openCollapse(event)}
      >
        <Label marginRight={false} letterSpacing={0.63} size={20} top={0} bottom={0} color="#3d3d3d">
          {item.name}
        </Label>
        <Information data-tooltip={item.tooltip || 'tooltip'}>
          i
        </Information>
        {renderInput(item.idType)}

      </Header>
      {open ? (
        <Description>
          <Label lineHeight={25} size={13} top={0} bottom={0} color="#4d4c59">
            {item.description}
          </Label>
        </Description>
      ) : null}

    </Item>
  )
}

export default ItemSimulator
