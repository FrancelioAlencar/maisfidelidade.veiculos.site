import styled from 'styled-components'
import { colors } from '@/helpers/styles'

// eslint-disable-next-line import/prefer-default-export
export const Wrapper = styled.div`
  display: inline-block;
  width: ${({ width = '100%' }) => width};
  position: relative;

  &::before {
    content: "";
    width: 2px;
    height: 40px;
    background-color: #34303f;
    display: inline-block;
    vertical-align: middle;
    position: absolute;
    left: 40px;
    top: 50%;
    transform: translateY(-50%);
    margin-right: 20px;
  }
`

export const Label = styled.label`
  font-size: 12px;
  display: block;
  margin-bottom: 10px;
  position: relative;
`
export const InputWrapper = styled.input`
  width: 100%;
  padding: 20px 40px 20px 60px;
  height: 80px;
  background: ${colors.white};
  border-radius: 10px;
  border: 0;
  font-size: 20px;
  color: #5e5e6a;
  font-family: 'Open Sans', sans-serif;


  &::placeholder {
    color: #5e5e6a61;
    opacity: 1;
    font-weight: bold;
  }
`
