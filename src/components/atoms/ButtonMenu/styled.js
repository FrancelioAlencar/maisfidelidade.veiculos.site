import styled, { css, keyframes } from 'styled-components'
import { colors } from '@/helpers/styles'

const move = keyframes`
  0% {  transform: translateX(-100%); }
  100% {  transform: translateX(0); }
`

const animation = () =>
  css`
    ${move} .5s ease-out 0s 1;
  `

// eslint-disable-next-line import/prefer-default-export
export const Button = styled.a`
  width: ${({ active }) => (active && '90px') || '70px'};
  height: ${({ small, middle, big, active }) => (active && small) && '40px' || (!active && small) && '20px' || (active && middle) && '60px' || (!active && middle) && '40px' || (active && big) && '80px' || (!active && big) && '60px'};
  line-height: ${({ small, middle, big, active }) => (active && small) && '40px' || (active && middle) && '60px' || (active && big) && '40px'};
  display: block;
  padding: 20px;
  background: ${({ active }) => (active && `${colors.primaryColor}`) || `${colors.white}`};
  text-align: center;
  position: relative;
  z-index: 1;
  border-bottom-right-radius: ${({ active }) => (active && '20px') || '0'};
  border-top-right-radius: ${({ active }) => (active && '20px') || '0'};
  backdrop-filter: blur(30px);
  border-bottom: 1px solid;
  animation:  ${({ active }) => (active && animation) || '0'};

  &:last-child {
    border-bottom: none;
  }

  svg {
    fill:  ${({ active }) => (active && `${colors.white}`) || `${colors.primaryColorHeavy}`};
    transform:  ${({ active }) => active ? 'scale(1.5)' : 'none'};
    position: relative;
    left: ${({ active }) => (active && '-10px' || 'auto')}
  }

  &:hover {
    border-left: ${({ active }) => (active && 'none') || `5px solid ${colors.primaryColor}`};
    transition:all 0.4s ease 0s;

    svg {
      fill: ${({ active }) => (active && `${colors.white}`) || `${colors.primaryColor}`};
      transform: ${({ active }) => (active && 'scale(1.5)') || 'none'}
    }
  }
`
