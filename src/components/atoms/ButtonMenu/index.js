import React from 'react'
import { func, string } from 'prop-types'

import { Button } from './styled'

const ButtonMenu = ({ click, color, small, route, active, middle, big, ...props }) => (
  <Button
    href={route}
    onClick={click}
    small={small}
    middle={middle}
    active={active}
    big={big}
    {...props}
  >
    { props.children }
  </Button>
)

ButtonMenu.propTypes = {
  click: func.isRequired,
  color: string,
  size: string,
  route: string.isRequired
}

ButtonMenu.defaultProps = {
  size: 'middle',
  color: 'primary'
}

export default ButtonMenu
