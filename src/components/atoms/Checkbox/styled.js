import styled from 'styled-components'

export const Container = styled.div`
  position: relative;
  overflow: hidden;
  width: 40px;
  height: 20px;
  background: transparent;
  border-radius: 100px;
  display: inline-block;
  vertical-align: middle;
  border: ${({ check }) => check ? '1px solid #f60535' : '1px solid #FFFFFF'};


  .knobs {
    z-index: 2;

    &:before {
      content: "";
      position: absolute;
      width: 18px;
      height: 18px;
      border-radius: 50%;
      background: #FFFFFF;
      transition: 0.3s ease all, left 0.3s cubic-bezier(0.18, 0.89, 0.35, 1.15);
      display: inline-block;
      left: 0;
      top: 0;
    }
  }

  .layer {
    width: 100%;
    background-color: #ebf7fc;
    transition: 0.3s ease all;
    z-index: 1;
  }

  input[type=checkbox]:active + .knobs:before {
    width: 46px;
    border-radius: 100px;
  }

  input[type=checkbox]:checked:active + .knobs:before {
    margin-left: -26px;
  }

  input[type=checkbox]:checked + .knobs:before {
    content: '';
    left: 20px;
    background-color: #f60535;
  }
`
export const InputCheck = styled.input`
  position: relative;
  width: 100%;
  height: 100%;
  padding: 0;
  margin: 0;
  opacity: 0;
  cursor: pointer;
  z-index: 3;
`
