import React from 'react'
import { Container, InputCheck } from './styled'

const Checkbox = ({ click, check }) => (
  <Container check={check} onClick={click}>
    <InputCheck type="checkbox" checked={check} />
    <span className="knobs" />
    <span className="layer" />
  </Container>
)

export default Checkbox
