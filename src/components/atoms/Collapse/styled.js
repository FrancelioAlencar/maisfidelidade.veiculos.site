import styled from 'styled-components'

export const Wrapper = styled.div`
  transition: all 0.5s ease-in-out;
`

export const WrapperAccordion = styled.div`
  display: block;
  cursor: pointer;
  margin-bottom: 20px;
  position: relative;
  outline: none;
  transition: all 0.5s ease-in-out;
  background: #FFF;
  border-radius: 10px;
`
export const AccordionTitle = styled.div`
  display: block;
  color: #4d4c59;
  padding: 22px 20px 0;
  border-radius: 4px 4px 0px 0px;
  font-size: 15px;
  font-family: 'Open Sans', sans-serif;
  font-weight: bold;
`

export const Title = styled.span`
  width: 100%;
  display: block;
  padding-bottom: 20px;
  border-bottom: ${({ active }) => active ? '1px solid #f0f0f0' : 'none' }
`

export const AccordionContent = styled.div`
  padding: 20px 15px;
  border-radius: 0px 0px 4px 4px;
  display: ${props => props.active ? 'block' : 'none' };
  overflow: hidden;
  transition: all 0.5s ease-in-out;
  font-size: 12px;
  color: #4d4c59;
  font-family: 'Open Sans', sans-serif;
`

export const Empty = styled.div`
  text-align: center;
`

// export const Arrow = styled(Icon)`
//   width: 15px;
//   height: 15px;
//   position: absolute;
//   right: 20px;
//   top: 23px;
//   transform: ${props => props.active ? 'rotate(90deg)' : 'rotate(270deg)' };
// `
