import React, { useState } from 'react'
import { Label } from '@/components/atoms'
import { Wrapper, WrapperAccordion, AccordionTitle, AccordionContent, Empty, Title } from './styled'

const Collapse = ({ data }) => {
  const [activeState, setActiveState] = useState(0)

  const onHandleClickOpen = id => setActiveState(id !== activeState ? id : null)

  return (
    <Wrapper>
      {data && data.map(item => (
        <WrapperAccordion key={item.title} onClick={() => onHandleClickOpen(item.id)}>
          {/* <Arrow id="arrow" active={item.id === activeState ? true : false} /> */}
          <AccordionTitle id={item.id}>
            <Title active={item.id === activeState ? true : false}>{item.title}</Title>
          </AccordionTitle>
          <AccordionContent active={item.id === activeState ? true : false}>
            {item.content}
          </AccordionContent>
        </WrapperAccordion>
      ))}
      {data.length === 0 && (
        <Empty>
          <Label size={25} weight="bold">
            Não foi encontrado nada relacionado a sua busca!
          </Label>
        </Empty>
      )}
    </Wrapper>
  )
}

export default Collapse
