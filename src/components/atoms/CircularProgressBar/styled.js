import styled from 'styled-components'
import { sizeVw } from '@/helpers/utils'

export const Container = styled.section`
    width: 100%;
    position: relative;
`

export const ContainerChildren = styled.section`
    position: absolute;
    width:50%;
    height: 50%;

    left:25%;
    top:40%;

    display: flex;
    flex-direction: column;
    align-item: center;
    justify-content: flex-end;
    text-align: center;
`