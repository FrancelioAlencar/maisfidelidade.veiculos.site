import React, { useState, useEffect } from 'react'
import './styles.scss'


const CircularProgressbar = ({ value }) => {
  const [valueChart, setValueChart] = useState(value || 0) 
  useEffect(() => {
    setValueChart(value)
  }, [value])
  return (
    <svg viewBox="0 0 110 110">
      <defs>
        <linearGradient id="gradient" x1="0%" y1="0%" x2="0%" y2="100%">
          <stop offset="0%" stopColor="#c39a52" />
          <stop offset={`${value * 1.3}%`} stopColor="#b8b7c3" />
        </linearGradient>
      </defs>
      <path
        className="CircularProgressbar-trail" 
        d="
            M 50,50
            m 0,-46
            a 46,46 0 1 1 0,92
            a 46,46 0 1 1 0,-92
        "
        strokeWidth="8"
        fillOpacity="0"
      />
      <path
        className="CircularProgressbar-path" 
        d="
        M 50,50
        m 0,-46
        a 46,46 0 1 1 0,92
        a 46,46 0 1 1 0,-92
        "
        stroke="url(#gradient)"
        strokeWidth={8}
        fillOpacity="0"
        strokeDashoffset={289.0276 - (216.7704 * (valueChart / 100))}
      />
      {/* <g id="circle" stroke="black">
        <circle cx="55" cy="55" r="5" fill="red" />
      </g> */}
      <path
        id="circle"
        d="
            M 55, 55
            m -50, 0
            a 5,5 0 1,0 10,0
            a 5,5 0 1,0 -10,0
        "
        transform="rotate(60, 0, 0)"
      />

      {/* <use xlinkHref="circle" transform="rotate(60, 50, 50)" /> */}
    </svg>
  )
}

export default CircularProgressbar
