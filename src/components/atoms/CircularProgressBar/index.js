import React, { useState, useEffect } from 'react'
import { Container, ContainerChildren } from './styled'
import CircularProgressbar from './CircularProgressBar'

const CircularProgressBar = ({ children = null, percentage = 0 }) => {
    const [valueChart, setValueChart] = useState(percentage)
    useEffect(() => {
        setValueChart(percentage)
    }, [percentage])

    return (
        <Container>
            <CircularProgressbar value={valueChart} />
            {
                children ?
                    <ContainerChildren >
                        {children}
                    </ContainerChildren>
                    : null
            }
        </Container >
    )
}

export default CircularProgressBar
