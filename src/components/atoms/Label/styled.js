import styled from 'styled-components'
import { colors } from '@/helpers/styles'
import { sizeVw } from '@/helpers/utils'

// const sizeVw = size => (size * 100) / 1920

export const Item = styled.span`
  display: ${({ display }) => display ? `${display}` : 'block'};
  vertical-align: ${({ display }) => display ? 'middle' : 'initial'};
  font-size: ${({ size }) => size ? `${sizeVw(size)}vw` : '10px'};
  font-weight: ${({ weight }) => weight || 'normal'};
  color: ${({ color }) => color || colors.primaryColorHeavy};
  width: ${({ width }) => width ? `${width}px` : 'auto'};
  line-height: ${({ lineHeight }) => lineHeight ? `${lineHeight}px` : 'initial'};
  font-family: 'Open Sans', sans-serif;
  margin-right: ${({ display, marginRight }) => display && marginRight !== false ? `5px` : marginRight === false ? '0' : 'auto'};
  margin-bottom: ${({ bottom }) => bottom || bottom === 0 ? `${bottom}px` : 'auto'};
  margin-top: ${({ top }) => top || top === 0 ? `${top}px` : '0'};
  letter-spacing: ${({ letterSpacing }) => letterSpacing || letterSpacing === 0 ? `${letterSpacing}px` : '0'};
`
