import styled from 'styled-components'

const Arrow = styled.span`
  position: absolute;
  top: 50%;
  left: ${({ prev }) => prev ? `0px` : 'calc(100% - 4px)'};
  width: 10px;
  height: 10px;
  border-style: solid;
  border-width: 0 1.5px 1.5px 0;
  border-color: ${({ disabled }) => disabled ? `rgba(0,0,0,0)` : '#fff'};
  display: inline-block;
  padding: 6px;
  transform: ${({ prev }) => prev ? `rotate(135deg)` : 'rotate(-45deg)'};
  -webkit-transform: ${({ prev }) => prev ? `rotate(135deg)` : 'rotate(-45deg)'};
  cursor: ${({ disabled }) => disabled ? `initial` : 'pointer'};
  z-index: 1;
`

export default Arrow
