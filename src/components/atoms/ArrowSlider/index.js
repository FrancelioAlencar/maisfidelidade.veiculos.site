import React from 'react'
import Arrow from './styled'

const ArrowSlider = props => {
    const { className, onClick } = props
    const disabled = className.indexOf('disabled') !== -1
    const prev = className.indexOf('prev') !== -1
    return (
        <Arrow
            disabled={disabled}
            prev={prev}
            onClick={onClick}
        />
    )
}

export default ArrowSlider
