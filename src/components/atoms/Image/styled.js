import styled from 'styled-components';

export const Image = styled.img`
  width: ${({ width }) => (width && width) || 'auto'};
  height: ${({ height }) => (height && height) || 'auto'};
  display: ${({ block }) => block ? 'inline-block' : 'block'};
  vertical-align: ${({ block }) => block ? 'top' : 'initial'};
`
