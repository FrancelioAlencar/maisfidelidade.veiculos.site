import React from 'react'

const Image = ({ source, type, width, height, block }) => <img src={`./assets/img/static/${source}.${type}`} width={width} block={block} height={height} alt={source} />

export default Image
