import React, { useState, useEffect, Fragment } from 'react'
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd'
import moment from 'moment'
import { Label } from '@/components/atoms'
import Indicators from '@/request/indicators'
import { Container, WrapperFilter, WrapperContent, MonthCurrent, Card } from './styled'

moment.locale('pt-br')
const Indicator = () => {
  const [listIndicators, setListIndicators] = useState([])
  const [isLoading, setIsLoading] = useState(true)

  useEffect(() => {
    setIsLoading(true)
    Indicators.getIndicators().then(({ error, data }) => {
      if (!error) setListIndicators(data.Indicadores)
    }).finally(() => {
      setIsLoading(false)
    })
  }, [])

  const reorder = (list, startIndex, endIndex) => {
    const result = Array.from(list)
    const [removed] = result.splice(startIndex, 1)
    result.splice(endIndex, 0, removed)
    return result
  }

  const onDragEnd = result => {
    if (!result.destination) return false

    const item = reorder(
      listIndicators,
      result.source.index,
      result.destination.index
    )

    return setListIndicators(item)
  }

  const getListStyle = () => ({ width: '100%' })

  return (
    <Container>
      { isLoading && <div>Carregando ...</div> }
      { listIndicators && listIndicators.length > 0 && (
        <Fragment>
          <WrapperFilter>
            <Label size={19} lineHeight={19} weight="bold" color="#5e5e6a" display="inline-block">Indicadores</Label>
          </WrapperFilter>
          <WrapperContent>
            <MonthCurrent>
              <Label color="#ff0000" weight="bold" size={20} lineHeight={20} display="inline-block">
                { moment().format('MMMM')}
              </Label>
            </MonthCurrent>
            <DragDropContext onDragEnd={onDragEnd}>
              <Droppable droppableId="droppable">
                {(provided, snapshot) => (
                  <div
                    {...provided.droppableProps}
                    ref={provided.innerRef}
                    style={getListStyle(snapshot.isDraggingOver)}
                  >
                    {listIndicators && listIndicators.map((item, index) => (
                      <Draggable key={item.IndicadorEnum} draggableId={`${item.IndicadorEnum}`} index={index}>
                        {(provided, snapshot) => (
                          <Card
                            ref={provided.innerRef}
                            {...provided.draggableProps}
                            {...provided.dragHandleProps}
                            drag={snapshot.isDragging}
                            onClick={() => onHandleClick()}
                          >
                            {item.content}
                          </Card>
                        )}
                      </Draggable>
                    ))}
                    {provided.placeholder}
                  </div>
                )}
              </Droppable>
            </DragDropContext>
          </WrapperContent>
        </Fragment>
      )}
      {/*  */}
    </Container>
  )
}

export default Indicator
