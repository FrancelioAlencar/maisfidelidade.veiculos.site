import styled from 'styled-components'
import { colors } from '@/helpers/styles'

export const Container = styled.section`
  width: 100%;
  margin-bottom: 20px
`

export const WrapperFilter = styled.div`
  width: 100%;
  background: ${colors.white};
  min-height: 80px;
  border-radius: 10px;
  margin-bottom: 20px;
  padding: 20px;
  line-height: 40px;
`
export const WrapperContent = styled.div`
  width: 100%;
  display: block;
`
export const MonthCurrent = styled.div`
  width: 100%;
  display: block;
  margin-bottom: 20px;

  &:before {
    content: "Mês Atual:";
    font-size: 20px;
    line-height: 20px;
    color: #5e5e6a;
    display: inline-block;
    vertical-align: middle;
    margin-right: 5px;
    font-family: 'Open Sans',sans-serif;
  }
`

export const Card = styled.div`
  background: ${({ drag }) => drag ? 'gray' : colors.white};
  width: calc(100% / 3 - 20px);
  display: inline-block;
  vertical-align: middle;
  backdrop-filter: blur(30px);
  border-radius: 10px;
  padding: 20px;
  margin: 0 10px 20px;
`
