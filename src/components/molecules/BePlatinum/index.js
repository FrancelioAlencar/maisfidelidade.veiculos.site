import React, { useState, useEffect, Fragment } from 'react'
import { connect } from 'react-redux'
import { Label, Checkbox } from '@/components/atoms'
import { Container, WrapperAction } from './styled'
import actionBePlatinum from '@/redux/actions/platinum'
import ResaleData from '@/request/resaleData'

const BePlatinum = ({ ...props }) => {
  const [check, setCheck] = useState(false)
  const [isPlatinum, setIsPlatinum] = useState(false)

  useEffect(() => {
    if (props.platinum.status || localStorage.getItem('BE_PLATINUM') === 'true') {
      setCheck(true)
    } else {
      setCheck(false)
    }

    ResaleData.getData().then(({ error, data }) => {
      if (!error) setIsPlatinum(data.AcaoSejaPlatinum)
    }).catch(e => {
      console.error(e)
    })
  }, [])

  useEffect(() => {
    if (check || localStorage.getItem('BE_PLATINUM') === 'true') {
      props.dispatch(actionBePlatinum(true))
    } else {
      props.dispatch(actionBePlatinum(false))
    }
  }, [check])

  const onHandleClick = () => {
    props.dispatch(actionBePlatinum(!check))
    localStorage.setItem('BE_PLATINUM', !check)
    setCheck(!check)
  }

  return (
    <Fragment>
      {isPlatinum && (
        <Container>
          <Label weight="bold" color="#FFFFFF" size={16} display="inline-block" lineHeight={17}>Parabéns!</Label>
          <Label color="#FFFFFF" size={16} display="inline-block" lineHeight={17}>Você foi selecionado para ter a experiência de ser um cliente!</Label>
          <Label weight="bold" color="#FFFFFF" size={20} display="inline-block" lineHeight={22}>Platinum</Label>
          <WrapperAction>
            <Checkbox click={() => onHandleClick()} check={check} />
          </WrapperAction>
        </Container>
      )}
    </Fragment>
  )
}

const mapStateToProps = state => ({
  platinum: state.platinum
})

export default connect(mapStateToProps, null)(BePlatinum)
