import styled from 'styled-components'

export const Container = styled.section`
  height: 60px;
  border-radius: 10px;
  background-color: #2e2b37;
  padding: 20px 40px;
  position: relative;
  margin-bottom: 20px;
`

export const WrapperAction = styled.div`
  position: absolute;
  right: 40px;
  top: 50%;
  transform: translateY(-50%);
`
