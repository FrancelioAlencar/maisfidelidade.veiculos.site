import React, { useState, useEffect } from 'react'

import {
  Container,
  Label,
  Filters,
  ItemFilter
} from './styled'

const Filter = ({ filters, handleChangeFilters = null }) => {
  const [lstFilters, setLstFilters] = useState(filters)

  const handleChange = (event, id) => {
    event.preventDefault()
    if (handleChangeFilters) handleChangeFilters(event, id)
  }

  useEffect(() => {
    setLstFilters(filters)
  }, [filters])

  return (
    <Container>
      <Label size={20} color="#5e5e6a" weight="bold">Filtrar:</Label>
      <Filters>
        {lstFilters.map(filter => (
          <ItemFilter active={filter.active} onClick={e => handleChange(e, filter.id)}>
            <Label lineHeight={14} size={10} color={filter.active ? '#fff' : '#5e5e6a'}>{filter.nome}</Label>
            {filter.active ? <Label pLeft={19} lineHeight={13} size={9} color="#fff">X</Label> : null}
          </ItemFilter>
        ))}

      </Filters>
    </Container>
  )
}
export default Filter
