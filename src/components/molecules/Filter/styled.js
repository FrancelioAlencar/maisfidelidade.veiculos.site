import styled from 'styled-components'
import { sizeVw } from '@/helpers/utils'

export const Container = styled.section`
  width: 100%;
  min-height: 80px;

  margin-bottom: 20px;
  
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  background-color: #fff;
  border-radius: 10px;
  padding: 20px ${sizeVw(40)}vw;
  position: relative;

  &:after {
    content: "";
    width: 0;
    height: 0;
    border-top: 25px solid transparent;
    border-left: 20px solid #FFF;
    border-bottom: 25px solid transparent;
    position: absolute;
    left: ${sizeVw(60)}vw;
    top: calc(100% - 15px);
    transform: rotate(90deg);
    margin-bottom: 10px;
  }
`

export const Label = styled.span`
  font-size: ${({ size }) => size ? `${sizeVw(size)}vw` : '10px'};
  font-weight: ${({ weight }) => weight || 'normal'};
  color: ${({ color }) => color || 'black'};
  line-height: ${({ lineHeight }) => lineHeight ? `${lineHeight}px` : 'initial'};
  font-family: 'Open Sans', sans-serif;
  padding-left: ${({ pLeft }) => pLeft ? `${sizeVw(pLeft)}vw` : '0'};
  margin-top: ${({ top }) => top ? `${top}px` : '0'};
  width: ${({ width }) => width ? `${sizeVw(width)}vw` : 'auto'};
  letter-spacing: ${({ letterSpacing }) => letterSpacing ? `${letterSpacing}px` : 'auto'};
`

export const Filters = styled.div`
  display: flex;
  flex: 1;
  flex-direction: row-reverse;
  flex-wrap: wrap;
`

export const ItemFilter = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 0 ${sizeVw(30)}vw;
  height: 40px;
  border: ${({ active }) => active ? `none` : '1px solid #707070'};
  background: ${({ active }) => active ? `linear-gradient(to bottom, #f9002d, #ce1435)` : 'none'}; 
  border-radius: 50px;
  cursor: pointer;

  &:not(:last-child) {
    margin-left: 19px;
  }
`
