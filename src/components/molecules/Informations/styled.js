import styled from 'styled-components';

export const Card = styled.div`
  background: #ffffff;
  padding: 40px;
  border-radius: 10px;
  margin-bottom: 20px;
`;
