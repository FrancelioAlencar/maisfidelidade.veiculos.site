import React, { Fragment } from 'react'
import { Label } from '@/components/atoms'
import { appMessages } from '@/helpers/utils'
import { Card } from './styled'

const Informations = () => {
  return (
    <Fragment>
      <Card>
        <Label size={20} color="#4d4c59" bottom={20} weight="bold" lineHeight={28}>{ appMessages.informations['title-about'] }</Label>
        <Label color="#d24147" size={12} bottom={20} weight="bold" lineHeight={20}>{ appMessages.informations.version }</Label>
        <Label color="#8b8c99" size={12}>{ appMessages.informations.description }</Label>
      </Card>
      <Label bottom={20} weight="bold" color="#4d4c59" size={15}>{ appMessages.informations.contact.title }</Label>
      <Card>
        <Label color="#d24147" size={14} bottom={5} weight="bold" lineHeight={19}>{ appMessages.informations.contact.company }</Label>
        <Label color="#4d4c59" size={14} bottom={5} weight="bold" lineHeight={15}>{ appMessages.informations.contact.phone }</Label>
        <Label color="#4d4c59" size={14} bottom={5} lineHeight={15}>{ appMessages.informations.contact.mail }</Label>
      </Card>
    </Fragment>
  )
}

export default Informations
