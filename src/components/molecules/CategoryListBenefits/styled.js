import styled from 'styled-components';

export const Container = styled.div`
  background: #FFFFFF;
  border-radius: 10px;
  padding: 40px;
  position: relative;
  border-top: ${({ border }) => border ? `5px solid ${border}` : 'none'};
`

export const CurrentImage = styled.div`
  display: inline-block;
  vertical-align: middle;
  margin-right: 10px;
`

export const Current = styled.div`
  display: inline-block;
  vertical-align: middle;
`

export const CurrentTitle = styled.div`
  color: #aeaebc;
  font-size: 90px;
  font-weight: bold;
  opacity: .16;
  position: absolute;
  top: 20px;
  right: 40px;
`

export const ContainerList = styled.div`
  width: 100%;
  overflow-y: hidden;
  overflow-x: scroll;
  position: relative;
`

export const Button = styled.button`
  background: #ff0010;
  color: #FFFFFF;
  position: absolute;
  top: 50%;
  width: 40px;
  height: 40px;
  transform: translateY(-50%);
  left: ${({ left }) => left ? '-20px' : 'auto'};
  right: ${({ right }) => right ? '-20px' : 'auto'};
  border-bottom-left-radius: ${({ left }) => left ? '10px' : '0'};
  border-top-left-radius: ${({ left }) => left ? '10px' : '0'};
  border-bottom-right-radius: ${({ right }) => right ? '10px' : '0'};
  border-top-right-radius: ${({ right }) => right ? '10px' : '0'};
`

export const List = styled.ul`
  margin-top: 60px;
  width: 120vw;
  display: inline-block;

  &:after {
    content: "";
    clear: both;
    display: table;
  }
`
export const Item = styled.li`
  border: solid 2px #d8d8da;
  background-color: #fdfdfd;
  color: #d8d8da;
  padding: 20px 100px 20px 20px;
  border-top-right-radius: 10px;
  border-bottom-right-radius: 10px;
  font-size: 15px;
  font-weight: bold;
  cursor: pointer;
  position: relative;
  margin: 0 10px 10px;
  float: left;
`

export const Percentage = styled.span`
  position: absolute;
  right: 0;
  height: 100%;
  top: 0;
  line-height: 65px;
  width: 30%;
  text-align: center;
  color: #fdfdfd;
`
export const Shape = styled.div`

`
