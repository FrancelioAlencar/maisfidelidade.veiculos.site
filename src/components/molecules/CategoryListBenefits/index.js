import React, { useEffect, useState } from 'react'
import { Image, Label } from '@/components/atoms'
import CategoryList from '@/request/categoryList'
import { Container, Current, CurrentImage, CurrentTitle, List, Item, Percentage, ContainerList, Button } from './styled'

const CategoryListBenefits = () => {
  const [category, setCategory ] = useState([])
  useEffect(() => {
    CategoryList.getAllBenefits(20).then(e => {
      setCategory(e)
    })
  }, [])

  return (
    <Container border="#aeaebb">
      <CurrentImage>
        <Image source="star-silver" type="png" width={60} height={60} block />
      </CurrentImage>
      <Current>
        <Label color="#2e2a37" size={18} lineHeight={25} display="inline-block">Categoria</Label>
        <Label color="#2e2a37" size={25} weight="bold" lineHeight={25} display="inline-block">Silver:</Label>
      </Current>
      <CurrentTitle>
        Silver
      </CurrentTitle>

      <ContainerList>
        <List>
          {category && category.map(i => {
            return (
              <Item>
                {i.content}
                <Percentage>
                  {i.percentage}
                  %
                </Percentage>
              </Item>
            )
          })}
        </List>
      </ContainerList>
      <Button left>ca</Button>
      <Button right>io</Button>
    </Container>
  )
}

export default CategoryListBenefits
