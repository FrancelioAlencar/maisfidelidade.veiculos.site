/* eslint-disable no-confusing-arrow */
import React, { Fragment, useState, useEffect } from 'react'
import { Filter } from '@/components/molecules'
import { Label, ItemSimulator } from '@/components/atoms'
import { Items } from './styled'

const startMock = [
  { id: 0, name: 'Market Share', idType: 1, idCompany: 1, value: 500, maxValue: 500, description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque fringilla vestibulum fermentum. Curabitur malesuada turpis quis maximus accumsan. Fusce pellentesque ac purus a laoreet. Phasellus massa tellus, placerat id tincidunt suscipit, vestibulum consequat augue. Aliquam cursus cursus ipsum et euismod. Vivamus quis mauris ac Leo hendrerit rutrum. Mauris dapibus, dui et finibus interdum, ante nunc aliquet nisl, non placerat justo purus id sem. Fusce vitae nibh rutrum, pretium lacus non, laoreet mauris. Quisque eget risus commodo, dictum dolor vitae, suscipit ante. ' },
  { id: 1, name: 'IP Seguro', idType: 1, idCompany: 1, value: 15, maxValue: 500, description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque fringilla vestibulum fermentum. Curabitur malesuada turpis quis maximus accumsan. Fusce pellentesque ac purus a laoreet. Phasellus massa tellus, placerat id tincidunt suscipit, vestibulum consequat augue. Aliquam cursus cursus ipsum et euismod. Vivamus quis mauris ac Leo hendrerit rutrum. Mauris dapibus, dui et finibus interdum, ante nunc aliquet nisl, non placerat justo purus id sem. Fusce vitae nibh rutrum, pretium lacus non, laoreet mauris. Quisque eget risus commodo, dictum dolor vitae, suscipit ante. ' },
  { id: 2, name: 'Pagamento C/C', idType: 1, idCompany: 1, value: 100, maxValue: 500, description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque fringilla vestibulum fermentum. Curabitur malesuada turpis quis maximus accumsan. Fusce pellentesque ac purus a laoreet. Phasellus massa tellus, placerat id tincidunt suscipit, vestibulum consequat augue. Aliquam cursus cursus ipsum et euismod. Vivamus quis mauris ac Leo hendrerit rutrum. Mauris dapibus, dui et finibus interdum, ante nunc aliquet nisl, non placerat justo purus id sem. Fusce vitae nibh rutrum, pretium lacus non, laoreet mauris. Quisque eget risus commodo, dictum dolor vitae, suscipit ante. ' },
  { id: 3, name: 'Aceite de Leads', idType: 1, idCompany: 0, value: 50, maxValue: 500, description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque fringilla vestibulum fermentum. Curabitur malesuada turpis quis maximus accumsan. Fusce pellentesque ac purus a laoreet. Phasellus massa tellus, placerat id tincidunt suscipit, vestibulum consequat augue. Aliquam cursus cursus ipsum et euismod. Vivamus quis mauris ac Leo hendrerit rutrum. Mauris dapibus, dui et finibus interdum, ante nunc aliquet nisl, non placerat justo purus id sem. Fusce vitae nibh rutrum, pretium lacus non, laoreet mauris. Quisque eget risus commodo, dictum dolor vitae, suscipit ante. ' },
  { id: 4, name: 'Estoque Potêncial', idType: 1, idCompany: 0, value: 25, maxValue: 500, description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque fringilla vestibulum fermentum. Curabitur malesuada turpis quis maximus accumsan. Fusce pellentesque ac purus a laoreet. Phasellus massa tellus, placerat id tincidunt suscipit, vestibulum consequat augue. Aliquam cursus cursus ipsum et euismod. Vivamus quis mauris ac Leo hendrerit rutrum. Mauris dapibus, dui et finibus interdum, ante nunc aliquet nisl, non placerat justo purus id sem. Fusce vitae nibh rutrum, pretium lacus non, laoreet mauris. Quisque eget risus commodo, dictum dolor vitae, suscipit ante. ' },
  { id: 5, name: 'Indicação CC/PJ', idType: 1, idCompany: 0, value: 10, maxValue: 500, description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque fringilla vestibulum fermentum. Curabitur malesuada turpis quis maximus accumsan. Fusce pellentesque ac purus a laoreet. Phasellus massa tellus, placerat id tincidunt suscipit, vestibulum consequat augue. Aliquam cursus cursus ipsum et euismod. Vivamus quis mauris ac Leo hendrerit rutrum. Mauris dapibus, dui et finibus interdum, ante nunc aliquet nisl, non placerat justo purus id sem. Fusce vitae nibh rutrum, pretium lacus non, laoreet mauris. Quisque eget risus commodo, dictum dolor vitae, suscipit ante. ' },
  { id: 6, name: 'Máquina GetNet', idType: 2, idCompany: 0, value: 0, maxValue: 500, description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque fringilla vestibulum fermentum. Curabitur malesuada turpis quis maximus accumsan. Fusce pellentesque ac purus a laoreet. Phasellus massa tellus, placerat id tincidunt suscipit, vestibulum consequat augue. Aliquam cursus cursus ipsum et euismod. Vivamus quis mauris ac Leo hendrerit rutrum. Mauris dapibus, dui et finibus interdum, ante nunc aliquet nisl, non placerat justo purus id sem. Fusce vitae nibh rutrum, pretium lacus non, laoreet mauris. Quisque eget risus commodo, dictum dolor vitae, suscipit ante. ' },
  { id: 7, name: 'Pagamento Webmotors', idType: 2, idCompany: 0, value: 100, maxValue: 500, description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque fringilla vestibulum fermentum. Curabitur malesuada turpis quis maximus accumsan. Fusce pellentesque ac purus a laoreet. Phasellus massa tellus, placerat id tincidunt suscipit, vestibulum consequat augue. Aliquam cursus cursus ipsum et euismod. Vivamus quis mauris ac Leo hendrerit rutrum. Mauris dapibus, dui et finibus interdum, ante nunc aliquet nisl, non placerat justo purus id sem. Fusce vitae nibh rutrum, pretium lacus non, laoreet mauris. Quisque eget risus commodo, dictum dolor vitae, suscipit ante. ' },
]

const filtersMock = [
  { id: 0, nome: 'FINANCEIRA', active: false },
  { id: 1, nome: 'WEBMOTORS', active: false }
]

const Simulator = ({ setValue, setMaxValue }) => {
  const [idCompanyFiltered, setIdCompanyFiltered] = useState([])
  const [mock, setMock] = useState(startMock)
  const [lstFilters, setLstFilters] = useState(filtersMock)

  useEffect(() => {
    getValueChart()
  }, [mock, lstFilters, idCompanyFiltered])

  const handleChange = (value, id) => {
    const newMock = mock.map(item => item.id === id ? { ...item, value: parseInt(value) } : item)
    setMock(newMock)
  }

  const handleChangeFilters = (event, id) => {
    event.preventDefault()
    const filters = lstFilters.map(
      filter => filter.id === id ? { ...filter, active: !filter.active } : filter
    )
    const idFiltered = filters.filter(filter => filter.active).map(filter => filter.id)
    setIdCompanyFiltered(idFiltered)
    setLstFilters(filters)
  }

  const getValueChart = () => {
    let valueChart = 0
    let maxValueChart = 0

    mock.filter(item => idCompanyFiltered.length > 0 ? idCompanyFiltered.includes(item.idCompany) : true)
      .forEach(item => {
        const { value } = item
        valueChart += value
      })

    mock.forEach(item => {
      const { maxValue } = item
      maxValueChart += maxValue
    })

    setValue(valueChart)
    setMaxValue(maxValueChart)
  }

  return (
    <Fragment>
      <Filter filters={lstFilters} handleChangeFilters={handleChangeFilters} />
      <Label size={13} top={30} bottom={20}>
        Utilize as barras para simular a pontuação necessária para alcançar as próximas categorias.
      </Label>
      <Items>
        {
          mock.filter(item => idCompanyFiltered.length > 0 ? idCompanyFiltered.includes(item.idCompany) : true)
            .map(item => <ItemSimulator item={item} onChange={handleChange} />)
        }
      </Items>
    </Fragment>
  )
}


export default Simulator
