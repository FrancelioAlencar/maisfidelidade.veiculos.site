import styled from 'styled-components'
import { sizeVw } from '@/helpers/utils'

export const Items = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  width: 102%;
  margin-left: -1%;
`