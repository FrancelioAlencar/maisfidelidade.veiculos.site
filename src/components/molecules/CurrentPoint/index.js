import React, { Fragment, useState, useEffect } from 'react'
import { Label, ArrowSlider } from '@/components/atoms'
import MyRating from '@/request/myRating'

import {
  Container,
  WrapperCollapse,
  Button, Collapse,
  CategoryCurrent,
  Point, Current,
  NextCategory,
  NextPoint,
  Circle,
  NextCurrent,
  Arrow,
  Star,
  BlockContainer,
  Loading,
  CollapsedCategory,
  CollapsedContent,
  CloseCollapse,
  ContainerArrow,
  Activities,
  Benefits,
  DivBenefits,
  LabelCollapse,
  ProgressBar,
  ItemSlider
} from './styled'
import Slider from 'react-slick'


let lst = [
  'Esta participando da campanha seja platinum.',
  'Você ganhou 02 vouchers para o cinema.saiba mais'
]

const lstBenefits = [
  { name: '+ Proteção Premiada', benefit: '10Pts' },
  { name: 'CMR', benefit: '10%' },
  { name: 'Feirao WebMotors', benefit: '10%' },
  { name: 'Blindagem', benefit: '10%' },
  { name: 'GearBox', benefit: '5%' }
]

const settings = {
  dots: false,
  infinite: false,
  speed: 500,
  rows: 1,
  slidesToShow: 1,
  slidesToScroll: 1,
  prevArrow: <ArrowSlider />,
  nextArrow: <ArrowSlider />
}

const CurrentPoint = ({ title }) => {
  const [isOpen, setIsOpen] = useState(false)
  const [isLoading, setIsLoading] = useState(true)
  const [rating, setRating] = useState({})
  const [colors] = useState({
    Basic: '#c68c50',
    Silver: '#aeaeba',
    Gold: '#ffce35',
    Platinum: '#696977'
  })

  useEffect(() => {
    setIsLoading(true)
    MyRating.getRating().then(({ error, data }) => {
      if (!error) setRating(data)
    }).finally(() => {
      setIsLoading(false)
    })
  }, [])

  return (
    <Container>
      {!isOpen ? (
        <Fragment>
          <Label size={25} bottom={0} width={131} display="inline-block" lineHeight={25} color="#5e5e6a">{title ? title : 'Pontuação Atual'}</Label>
          <WrapperCollapse>
            <Circle size={80} color={rating.NomeCategoriaAtual ? colors[rating.NomeCategoriaAtual] : colors['Basic']}>
              <Star />
            </Circle>
            <CategoryCurrent>
              <Current>
                <Label size={14} color="#FFFFFF" lineHeight={16}>CATEGORIA ATUAL</Label>
                <Label size={24} color="#FFFFFF" weight="bold">{rating.NomeCategoriaAtual || 'Basic'}</Label>
              </Current>
              <Point>
                {rating.TotalPontos || 0}
              </Point>
            </CategoryCurrent>
            <NextCategory>
              <Circle size={40} left={33} color={rating.NomeProximaCategoria ? colors[rating.NomeProximaCategoria] : colors['Silver']}>
                <Star size={20} />
              </Circle>
              <NextCurrent>
                <Label size={12} lineHeight={12} bottom={0} color="#FFFFFF">SEJA</Label>
                <Label size={22} lineHeight={22} bottom={-2} color="#FFFFFF" weight="bold">{rating.NomeProximaCategoria || 'Silver'}</Label>
              </NextCurrent>
              <NextPoint>
                +
                {rating.PontosNecessarios || 0}
              </NextPoint>
            </NextCategory>
            <Button size={14} color="#f3f5fa" weight="bold" onClick={() => setIsOpen(!isOpen)}>VER MEUS BENEFÍCIOS</Button>
            <ContainerArrow onClick={() => setIsOpen(!isOpen)}>
              <Arrow />
            </ContainerArrow>
          </WrapperCollapse>
        </Fragment>
      ) : (
        <Collapse>
          <CollapsedCategory size={520}>
            <Slider
              {...settings}
            >
              <ItemSlider>
                <Circle position="relative" typeVw top={0} left={0} size={120} color={rating.NomeCategoriaAtual ? colors[rating.NomeCategoriaAtual] : colors['Basic']}>
                  <Star typeVw size={60} />
                </Circle>
                <LabelCollapse top={21} weight="bold" size={15} color="#FFFFFF">CATEGORIA ATUAL</LabelCollapse>
                <LabelCollapse weight="bold" size={30} color="#FFFFFF">{rating.NomeCategoriaAtual || 'Basic'}</LabelCollapse>
                <ProgressBar position={((rating.TotalPontos || 0) * 100) / ((rating.TotalPontos || 1500) + (rating.PontosNecessarios || 0))} />
                <LabelCollapse top={3} letterSpacing={0.75} width={280} lineHeight={20} size={15} color="#FFFFFF">
                  Faltam <b>{rating.PontosNecessarios || 0}</b> pontos para subir pra categoria <b>{rating.NomeProximaCategoria || 'Silver'}</b>
                </LabelCollapse>
              </ItemSlider>
              <ItemSlider>
                <Circle position="relative" typeVw top={0} left={0} size={120} color={rating.NomeCategoriaAtual ? colors[rating.NomeCategoriaAtual] : colors['Silver']}>
                  <Star typeVw size={60} />
                </Circle>
                <LabelCollapse top={21} weight="bold" size={15} color="#FFFFFF">CATEGORIA ATUAL</LabelCollapse>
                <LabelCollapse weight="bold" size={30} color="#FFFFFF">{rating.NomeCategoriaAtual || 'Silver'}</LabelCollapse>
                <ProgressBar position={((rating.TotalPontos || 0) * 100) / ((rating.TotalPontos || 1500) + (rating.PontosNecessarios || 0))} />
                <LabelCollapse top={3} letterSpacing={0.75} width={280} lineHeight={20} size={15} color="#FFFFFF">
                  Faltam <b>{rating.PontosNecessarios || 0}</b> pontos para subir pra categoria <b>{rating.NomeProximaCategoria || 'Gold'}</b>
                </LabelCollapse>
              </ItemSlider>
              <ItemSlider>
                <Circle position="relative" typeVw top={0} left={0} size={120} color={rating.NomeCategoriaAtual ? colors[rating.NomeCategoriaAtual] : colors['Gold']}>
                  <Star typeVw size={60} />
                </Circle>
                <LabelCollapse top={21} weight="bold" size={15} color="#FFFFFF">CATEGORIA ATUAL</LabelCollapse>
                <LabelCollapse weight="bold" size={30} color="#FFFFFF">{rating.NomeCategoriaAtual || 'Gold'}</LabelCollapse>
                <ProgressBar position={((rating.TotalPontos || 0) * 100) / ((rating.TotalPontos || 1500) + (rating.PontosNecessarios || 0))} />
                <LabelCollapse top={3} letterSpacing={0.75} width={280} lineHeight={20} size={15} color="#FFFFFF">
                  Faltam <b>{rating.PontosNecessarios || 0}</b> pontos para subir pra categoria <b>{rating.NomeProximaCategoria || 'Platinum'}</b>
                </LabelCollapse>
              </ItemSlider>
              <ItemSlider>
                <Circle position="relative" typeVw top={0} left={0} size={120} color={rating.NomeCategoriaAtual ? colors[rating.NomeCategoriaAtual] : colors['Platinum']}>
                  <Star typeVw size={60} />
                </Circle>
                <LabelCollapse top={21} weight="bold" size={15} color="#FFFFFF">CATEGORIA ATUAL</LabelCollapse>
                <LabelCollapse weight="bold" size={30} color="#FFFFFF">{rating.NomeCategoriaAtual || 'Platinum'}</LabelCollapse>
                <ProgressBar position={((rating.TotalPontos || 0) * 100) / ((rating.TotalPontos || 1500) + (rating.PontosNecessarios || 0))} />
                <LabelCollapse top={3} letterSpacing={0.75} width={280} lineHeight={20} size={15} color="#FFFFFF">
                  Faltam <b>{rating.PontosNecessarios || 0}</b> pontos para subir pra categoria <b>{rating.NomeProximaCategoria || 'Platinum'}</b>
                </LabelCollapse>
              </ItemSlider>
            </Slider>
          </CollapsedCategory>

          <CollapsedContent>
            <CloseCollapse>
              <Button size={14} lineHeight={28} color="#f3f5fa" weight="bold" onClick={() => setIsOpen(!isOpen)}>FECHAR BENEFÍCIOS</Button>
              <ContainerArrow onClick={() => setIsOpen(!isOpen)}>
                <Arrow />
              </ContainerArrow>
            </CloseCollapse>
            <Label weight="bold" size={25} lineHeight={20} bottom={0} color="#FFFFFF">Atividades:</Label>
            <Activities>
              {lst.map(text => <Label size={18} lineHeight={22} bottom={0} color="#969696">{`- ${text}`}</Label>)}
            </Activities>
            <Label weight="bold" size={20} lineHeight={20} bottom={0} color="#FFFFFF">Benefícios:</Label>
            <DivBenefits>
              {lstBenefits.map(benefit => (
                <Benefits>
                  <LabelCollapse size={15} lineHeight={22} color="#969696">{`${benefit.name}`}</LabelCollapse>
                  <LabelCollapse weight="bold" point size={15} lineHeight={22} color="#FFFFFF">{`${benefit.benefit}`}</LabelCollapse>
                </Benefits>
              ))}
            </DivBenefits>
          </CollapsedContent>
        </Collapse>
      )
      }

    </Container>
  )
  // :
  // (
  //   <Container>
  //     <BlockContainer>
  //       {isLoading && <Loading />}
  //     </BlockContainer>
  //   </Container>
  // )
}
export default CurrentPoint
