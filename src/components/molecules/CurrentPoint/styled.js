import styled from 'styled-components'
import { colors } from '@/helpers/styles'
import { sizeVw } from '@/helpers/utils'

export const Container = styled.section`
  width: 100%;
  margin-bottom: 20px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`

export const BlockContainer = styled.section`
  width: 100%;
  height: 80px;
  display: flex;
  flex-direction: row;
  align-items: center;
  border-radius: 10px;
  backdrop-filter: blur(30px);
  background: #2e2b37;
`

export const Loading = styled.span`
  width: 100%;
  display: flex;
  background: #9e978e;
`

export const WrapperCollapse = styled.div`
  display: flex;
  background: #2e2b37;
  height: 80px;
  width: 75%;
  vertical-align: middle;
  backdrop-filter: blur(30px);
  position: relative;
  border-radius: 10px;
  align-items: center;
  padding-right: ${sizeVw(40)}vw;

  &::before {
    position: absolute;
    left: -20px;
    top: -5px;
    width: 80px;
    height: 90px;
    border-radius:0 50% 50% 0 !important;
    background-color:#f3f5fb;    
    display:inline-block;
    content: '';
    z-index: 1;

  }
`

export const Circle = styled.span`
  position: ${({ position }) => position ? position : 'absolute'};
  left: ${({ left }) => left || left === 0 ? `${sizeVw(left)}vw` : '-30px'}
  top: ${({ size, top }) => size && top !== 0 ? `calc(50% - ${size / 2}px)` : '0'};

  height: ${({ size, typeVw }) => size && !typeVw ? `${size}px` : typeVw ? `${sizeVw(size)}vw` : '80px'};
  width: ${({ size, typeVw }) => size && !typeVw ? `${size}px` : typeVw ? `${sizeVw(size)}vw` : '80px'};
  border-radius: 50%;
  z-index: 2;
  background-color:  ${({ color }) => color || '#aeaeba'};
  display: flex;
  justify-content: center;
  align-items: center;

  &::before {
    content: "";
    display: inline-block;
    left: 50%;
    position: absolute;
    z-index: 2;
    width: 50%;
    height: 100%;
    border-bottom-right-radius: ${({ size }) => `${size * 2}px`};
    border-top-right-radius: ${({ size }) => `${size * 2}px`};
    background-color: rgba(0,0,0,.1);
  }
`

export const Star = styled.span`
  background-color: #fff;
  clip-path: polygon(50% 0%, 63% 31%, 98% 35%, 70% 57%, 79% 91%, 50% 73%, 21% 91%, 30% 58%, 2% 35%, 35% 32%);
  height: ${({ size, typeVw }) => size && !typeVw ? `${size}px` : typeVw ? `${sizeVw(size || 40)}vw` : '40px'};
  width: ${({ size, typeVw }) => size && !typeVw ? `${size}px` : typeVw ? `${sizeVw(size || 40)}vw` : '40px'};
`

export const Button = styled.label`
  font-size: ${sizeVw(14)}vw;
  color: #f3f5fa;
  font-weight: bold;
  font-family: 'Open Sans', sans-serif;
  cursor: pointer;
  flex: 1;
  text-align: right;
  line-height: ${({ lineHeight }) => lineHeight ? `${lineHeight}px` : 'initial'};
`

export const Arrow = styled.span`
  border: solid #fff;
  border-width: 0 1.5px 1.5px 0;
  display: inline-block;
  padding: 4px;
  transform: rotate(45deg);
  -webkit-transform: rotate(45deg);
  margin-top: -8px;
  margin-left: 5px;
  cursor: pointer;
`

export const ContainerArrow = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
`

export const Collapse = styled.div`
  display: flex;
  overflow: hidden;
  min-height: 300px;
  width: 100%;
  background: #2e2b37;
  border-radius: 10px;
  backdrop-filter: blur(30px);
  background-color: #2e2a37;
`

export const CollapsedCategory = styled.div`
  width: ${({ size }) => size ? `${sizeVw(size)}vw` : '440px'};
  height: auto;
  background-color: #1b1b1d;
  padding: 80px 40px;

  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  .slick-slider {
    width: 100%;
  }
`

export const ItemSlider = styled.div`
  flex:1;
  display: flex !important;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`

export const CloseCollapse = styled.div`
  width: 100%;
  display: flex;
  justify-content: flex-end;
`

export const Activities = styled.div`
  width: 100%;
  padding: 30px 0;
`

export const LabelCollapse = styled.span`
  font-size: ${({ size }) => size ? `${sizeVw(size)}vw` : '10px'};
  font-weight: ${({ weight }) => weight || 'normal'};
  color: ${({ color }) => color || colors.primaryColorHeavy};
  line-height: ${({ lineHeight }) => lineHeight ? `${lineHeight}px` : 'initial'};
  font-family: 'Open Sans', sans-serif;
  padding-left: ${({ point }) => point ? `${sizeVw(50)}vw` : '0'};
  margin-top: ${({ top }) => top ? `${top}px` : '0'};
  width: ${({ width }) => width ? `${sizeVw(width)}vw` : 'auto'};
  letter-spacing: ${({ letterSpacing }) => letterSpacing ? `${letterSpacing}px` : 'auto'};
`

export const ProgressBar = styled.span`
  width: ${sizeVw(280)}vw;
  height: 10.7px;
  background: linear-gradient(to right, #00d6a6, #73e8ce);
  border-radius: 10px;
  margin-top: 20.1px;

  position: relative;

  &:after {
    content: "";
    width: 16px;
    height: 16px;
    background-color: #71727f;
    border: 4px solid #FFFFFF;
    border-radius: 50%;

    position: absolute;
    left: ${({ position }) => `calc(${position}% - 8px)`};
    top: -3px;
    box-shadow: inset 0 0 1em gold, 0 0 1em red;
  }
`


export const Benefits = styled.div`
  min-width: ${sizeVw(160)}vw;
  height: 60px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 0 ${sizeVw(20)}vw;
  background-color: #43404b;
  
  margin-top: 20px;
  margin-right: 15px;


  border-top-right-radius: 10px;
  border-bottom-right-radius: 10px;
  border-left: 5px solid #9c9ca8;
`

export const DivBenefits = styled.div`
  display: flex;
  width: 100%;
  padding-right: ${sizeVw(200)}vw;
  flex-wrap: wrap;
`

export const ButtonClose = styled.div`
  cursos: pointer;
`

export const CollapsedContent = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  position: relative;
  padding: 32px 41px 60px 41px;
`

export const CategoryCurrent = styled.div`
  height: 100%;
  background: #1b1b1d;
  min-width: 35%;
  display:flex;
  align-items: center;
  position: relative;
  padding-right: ${sizeVw(20)}vw;

  &:after {
    content: "";
    width: 0;
    height: 0;
    border-top: 25px solid transparent;
    border-left: 20px solid #1b1b1d;
    border-bottom: 25px solid transparent;
    position: absolute;
    right: -20px;
    top: 50%;
    transform: translateY(-50%);
  }
`


export const Point = styled.p`
  font-size: ${sizeVw(50)}vw;
  font-weight: bold;
  color: #FFF;
  font-family: 'Open Sans', sans-serif;
  display: inline-block;
  vertical-align: middle;
  padding-left: ${sizeVw(60)}vw;

  &: after {
    content: "Pts";
    font-size: ${sizeVw(20)}vw;
    margin-top: 7.5px;
    vertical-align: top;
    display: inline-block;
    margin-left: 0px;
}
`

export const Current = styled.div`
  display: inline-block;
  padding-right: 10px;
  padding-left: 80px;
  vertical-align: middle;
`

export const NextCurrent = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  height: 100%;
`

export const NextCategory = styled.div`
  display: flex;
  align-items: center;
  max-width: 65%;
  height: 100%;
  position: relative;
  padding-left: calc(${sizeVw(65)}vw + 20px);
`

export const Icone = styled.div`
  position: absolute;
  left: -50px;
  top: -4px;
  box-sizing: border-box;
  z-index: 1;
  display: inline-block;
  vertical-align: middle;

  img {
    border: ${({ noBorder }) => noBorder ? '0' : `5px solid ${colors.white}`};
    border-radius: 50%;
  }
`
export const NextPoint = styled.p`
  margin-left: 30px;
  font-size: ${sizeVw(50)}vw;
  font-weight: bold;
  color: #FFF;
  font-family: 'Open Sans', sans-serif;
  display: inline-block;
  vertical-align: middle;
  position: relative;

  &:after {
    content: "Pts";
    font-size: ${sizeVw(20)}vw;
    vertical-align: top;
    display: inline-block;
    margin-top: 7.5px;
    margin-left: 5px;
  }

  &:before {
    content: "necessários";
    font-size: ${sizeVw(12)}vw;
    font-weight: normal;
    position: absolute;
    right: ${sizeVw(-35)}vw;
    bottom: 10px;
  }


`
