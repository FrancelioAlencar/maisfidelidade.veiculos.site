import React from 'react'
import { Container, Header } from './styled'
import { Label } from '@/components/atoms'

const Card = ({ laneId, dragging }) => (
  <Container laneId={laneId} dragging={dragging}>
  		<Header>
  			<div>
  				<Label 
  				size={20} 
  				color={'#3d3d3d'} 
  				lineHeight={29}
  				display={'block'}>
  					Market Share
  				</Label>
  				<Label 
  				size={16} 
  				color={'#696977'}
  				weight={'bold'}
  				display={'block'}>
  					Mês Atual
  				</Label>
  			</div>
  			<div className={'percent'}>
   				<Label 
  				size={59} 
  				color={'#696977'}
  				weight={'bold'}
  				vertical={'top'}
  				display={'inline'}>
  					33
  				</Label>  				
  				<Label 
  				size={26} 
  				color={'#696977'}
  				weight={'bold'}
  				vertical={'top'}
  				display={'inline'}>
  					%
  				</Label>
  			</div>
  		</Header>
  </Container>
)

export default Card