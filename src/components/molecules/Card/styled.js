import styled from 'styled-components'
import { colors } from '@/helpers/styles'

export const Header = styled.div`
	display: flex !important;

	div {
		display: block !important;
		flex: 1 !important; 
	}

	.percent {
		text-align: right;
		vertical-align: top;
	}
`

export const Container = styled.div`
  background: ${({ drag }) => drag ? 'gray' : colors.white};
  width: calc(100% / 3 - 20px);
  display: inline-block;
  vertical-align: middle;
  backdrop-filter: blur(30px);
  border-radius: 10px;
  padding: 20px;
  margin: 0 15px 20px 15px !important;

  ${Header}
`
