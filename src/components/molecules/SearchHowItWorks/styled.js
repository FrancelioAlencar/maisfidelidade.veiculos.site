import styled from 'styled-components'

// eslint-disable-next-line import/prefer-default-export
export const Container = styled.div`
  width: 100%;
  display: inline-block;
  vertical-align: middle;
`

export const FormWrapper = styled.form`
  position: relative;
  margin-bottom: 20px;
`

export const Button = styled.button`
  width: 60px;
  height: 40px;
  border-radius: 50px;
  background-color: #e02e41;
  position: absolute;
  right: 40px;
  top: 50%;
  transform: translateY(-50%);

  svg {
    fill: #FFF;
  }
`
