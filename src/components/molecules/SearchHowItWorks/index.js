import React, { useState, useEffect } from 'react'
import { common, appMessages } from '@/helpers/utils'
import { Input, Icon, Collapse } from '@/components/atoms'
import { Container, FormWrapper, Button } from './styled'

const SearchHowItWorks = () => {
  const [listCommon, setListCommon] = useState([])
  const [search, setSearch] = useState('')

  useEffect(() => {
    if (common) setListCommon(common)
  }, [])

  const onHandleChange = e => {
    e.preventDefault()
    if (search !== '') {
      const result = listCommon.filter(el => {
        const searchValue = el.content.toLowerCase()
        return searchValue.indexOf(search.toLowerCase()) !== -1
      })
      setListCommon(result)
    } else {
      setListCommon(common)
    }
  }

  return (
    <Container>
      <FormWrapper onSubmit={e => onHandleChange(e)}>
        <Input onChange={e => setSearch(e)} placeholder={appMessages['how-it-works'].placeholder} />
        <Button onClick={e => onHandleChange(e)}>
          <Icon id="search" small/>
        </Button>
      </FormWrapper>
      <Collapse data={listCommon} />
    </Container>
  )
}

export default SearchHowItWorks
