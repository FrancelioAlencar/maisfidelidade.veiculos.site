import React from 'react'
import Slider from 'infinite-react-carousel'
import moment from 'moment'
import { jwtGet } from 'webmotors-react-pj/utils'
import { Container, Item, Placeholder, WrapperClock, Wrapper, WrapperWelcome } from './styled'
import { Label, Clock } from '@/components/atoms'

moment.locale('pt-br')

const date = `${moment().format('DD')} de ${moment().format('MMMM')} de ${moment().format('YYYY')}`


const CarouselCampaing = () => (
  <Container>
    <Slider dots arrows={false}>
      <Placeholder>
        <WrapperClock>
          <Clock />
        </WrapperClock>
        <Wrapper>
          <WrapperWelcome>
            <Label size={40} display="inline-block" color="#34303f">Bem-Vindo</Label>
            <Label size={40} color="#34303f" weight="bold" display="inline-block">{ jwtGet().nameid },</Label>
          </WrapperWelcome>
          <Label size={60} color="#f60535" weight="bold" lineHeight={82}>{ date }</Label>
        </Wrapper>
      </Placeholder>
      {/* <Item>
        <h3>2</h3>
      </Item> */}
    </Slider>
  </Container>
)

export default CarouselCampaing
