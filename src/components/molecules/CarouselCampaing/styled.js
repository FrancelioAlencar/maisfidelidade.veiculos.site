import styled from 'styled-components'
import { colors } from '@/helpers/styles'

export const Container = styled.section`
  display: block;
  width: 100%;
  margin-bottom: 20px;

  .carousel-dots li {
    width: 20px;
    height: 20px;
  }

  .carousel-dots li button {
    width: 15px;
    height: 15px;
    padding: 0;
    margin: 0 auto;
    color: transparent;
  }

  .carousel-dots li button:before {
    font-size: 32px;
    line-height: 12px;
    border: 2px solid #f60535;
    color: transparent;
    width: 16px;
    height: 16px;
    border-radius: 50%;
  }

  .carousel-dots li.carousel-dots-active button:before {
    color: #f60535;
  }
`
export const Item = styled.div`
  min-height: 320px;
  border-radius: 10px;
  position: relative;
  cursor: grab;
`
export const Placeholder = styled.div`
  background: ${colors.white}
  min-height: 320px;
  border-radius: 10px;
  position: relative;
  cursor: grab;
`

export const Wrapper = styled.div`
  position: relative;
  left: calc(50% - 120px);
  transform: translateY(80%);
`

export const WrapperWelcome = styled.div`
  position: relative;
  left: -100px;
`
export const WrapperDate = styled.div``

export const WrapperClock = styled.div`
  position: absolute;
  right: 40px;
  top: 40px;
`
