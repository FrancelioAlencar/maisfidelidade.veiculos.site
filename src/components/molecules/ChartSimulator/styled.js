import styled from 'styled-components'
import { sizeVw } from '@/helpers/utils'

export const DivChartSimulator = styled.section`
    width: 100%;
    min-height: 300px;
    border-radius: 10px;

    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    background-color: #fff;
    padding: 20px ${sizeVw(60)}vw;
`

export const Label = styled.span`
  font-size: ${({ size }) => size ? `${sizeVw(size)}vw` : '10px'};
  font-weight: ${({ weight }) => weight || 'normal'};
  color: ${({ color }) => color || '#000000'};
  line-height: ${({ lineHeight }) => lineHeight ? `${lineHeight}px` : 'initial'};
  font-family: 'Open Sans';
  margin-top: ${({ top }) => top ? `${top}px` : '0'};
  margin-bottom: ${({ bottom }) => bottom ? `${bottom}px` : '0'};
  letter-spacing: ${({ letterSpacing }) => letterSpacing ? `${letterSpacing}px` : 'auto'};
  text-align: center;
  width:  ${({ width, percentage }) => width && !percentage ? `${sizeVw(width)}vw` : width && percentage ? `${width}%` : '0'};
`

export const Circle = styled.span`
  position: ${({ position }) => position ? position : 'absolute'};
  top: ${({ size, top }) => size && top !== 0 ? `calc(${top}% - ${size / 2}px)` : '0'};
  left: calc(50% - ${sizeVw(31)}vw);

  height: ${({ size, typeVw }) => size && !typeVw ? `${size}px` : typeVw ? `${sizeVw(size)}vw` : '80px'};
  width: ${({ size, typeVw }) => size && !typeVw ? `${size}px` : typeVw ? `${sizeVw(size)}vw` : '80px'};
  border-radius: 50%;
  z-index: 2;
  background-color:  ${({ color }) => color || '#aeaeba'};
  display: flex;
  justify-content: center;
  align-items: center;

  &::before {
    content: "";
    display: inline-block;
    left: 50%;
    position: absolute;
    z-index: 2;
    width: 50%;
    height: 100%;
    border-bottom-right-radius: ${({ size }) => `${size * 2}px`};
    border-top-right-radius: ${({ size }) => `${size * 2}px`};
    background-color: rgba(0,0,0,.1);
  }
`

