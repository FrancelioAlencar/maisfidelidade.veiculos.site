import React, { Fragment, useState, useEffect } from 'react'


import {
    DivChartSimulator,
    Label,
    Circle
} from './styled'
import { Star } from '../CurrentPoint/styled';
import CircularProgressbar from '@/components/atoms/CircularProgressBar';


const ChartSimulator = ({ value, maxValue }) => {
    const [percentage, setPercentage] = useState((100 * value || 0) / maxValue || 1)

    useEffect(() => {
        setPercentage((100 * value || 0) / maxValue || 1)
    }, [value, maxValue])

    return (
        <Fragment>
            <DivChartSimulator>
                <CircularProgressbar percentage={percentage}>
                    <Label width={100} percentage lineHeight={17} size={15} color="#222222">CATEGORIA:</Label>
                    <Label width={100} percentage weight="bold" lineHeight={40} bottom={8} size={45} color="#3b3649">Silver</Label>
                    <Circle size={62} position="relative" typeVw top={25} >
                        <Star size={31} />
                    </Circle>
                </CircularProgressbar>
                <Label width={300} lineHeight={21} letterSpacing={-0.33} size={13} color="#222222">Veja os pontos necessários para alcançar os próximos níveis de categoria. Esses pontos não se referem a sua pontuação e categoria atual.</Label>
            </DivChartSimulator>
        </Fragment>
    )
}

export default ChartSimulator
