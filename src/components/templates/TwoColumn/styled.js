import styled from 'styled-components'

export const Container = styled.section``

export const First = styled.div`
  width: calc(80% - 180px);
  display: inline-block;
  margin-right: 20px;
  vertical-align: text-top
`

export const Second = styled.div`
  width: calc(20% + 160px);
  display: inline-block;
  vertical-align: top;
`
