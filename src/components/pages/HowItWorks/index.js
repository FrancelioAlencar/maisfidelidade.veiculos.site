import React, { Fragment } from 'react'
import { CurrentPoint, SearchHowItWorks, Informations } from '@/components/molecules'
import { appMessages } from '@/helpers/utils'

import { TwoColumn } from '@/components/templates'

const HowItWorks = () => (
  <Fragment>
    <CurrentPoint title={appMessages['how-it-works'].title} />
    <TwoColumn columnOne={<SearchHowItWorks />} columnTwo={<Informations />} />
  </Fragment>
)

export default HowItWorks
