import React, { Fragment } from 'react'
import { Indicators } from '@/components/organims'
import { CurrentPoint, BePlatinum, CarouselCampaing, CategoryListBenefits } from '@/components/molecules'

const Dashboard = () => (
  <Fragment>
    <CurrentPoint title="Pontuação Atual" />
    <CarouselCampaing />
    <BePlatinum />
    <CategoryListBenefits />
    <Indicators />
  </Fragment>
)

export default Dashboard
