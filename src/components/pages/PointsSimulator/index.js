import React, { Fragment, useState } from 'react'
import { CurrentPoint, Simulator, ChartSimulator } from '@/components/molecules'

import { TwoColumn } from '@/components/templates'

const PointsSimulator = () => {
  const [value, setValue] = useState(0)
  const [maxValue, setMaxValue] = useState(0)
  
  return (
    <Fragment>
      <CurrentPoint title="Simulador de pontos" />
      <TwoColumn columnOne={<Simulator setValue={setValue} setMaxValue={setMaxValue} />} columnTwo={<ChartSimulator value={value} maxValue={maxValue} />} />
    </Fragment>
  )
}

export default PointsSimulator
