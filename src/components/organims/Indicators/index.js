import React, { Fragment, useState, useEffect } from 'react'
import Board from '@lourenci/react-kanban'
import { Card } from '@/components/molecules'
import './style.scss'

const initBoard = {
  lanes: [
    {
      id: 1,
      title: '',
      cards: [
        {
          id: 1,
          title: 'Add card',
          laneId: 1,
          description: 'Add capability to add a card in a lane'
        },
        {
          id: 3,
          title: '3 cardasduahsd huasdhuuha',
          laneId: 1,
          description: 'Add capability to add a card in a lane'
        },
      ]
    },
    {
      id: 2,
      title: '',
      cards: [
        {
          id: 2,
          title: 'Drag-n-drop support',
          laneId: 2,
          description: 'Move a card between the lanes'
        },
      ]
    },
    {
      id: 3,
      title: '',
      cards: [
        {
          id: 4,
          title: 'Drag-n-drop support',
          laneId: 3,
          description: 'Move a card between the lanes'
        },
      ]
    }
  ]
}

const Indicators = () => {
  const [board, setBoard] = useState(initBoard)

  const handleOnCardDragEnd = (card, destination, source) => {
    console.log('asdasd')
    setBoard({
  lanes: [
    {
      id: 1,
      title: '',
      cards: [
        {
          id: 3,
          title: '3 cardasduahsd huasdhuuha 323232',
          laneId: 1,
          description: 'Add capability to add a card in a lane'
        },
      ]
    },
    {
      id: 2,
      title: '',
      cards: [
        {
          id: 2,
          title: 'Drag-n-drop support',
          laneId: 2,
          description: 'Move a card between the lanes'
        },
      ]
    },
    {
      id: 3,
      title: '',
      cards: [
        {
          id: 4,
          title: 'Drag-n-drop support',
          laneId: 3,
          description: 'Move a card between the lanes'
        },
      ]
    }
  ]
})
  }

  const LaneAdder = () => {
    return (
      <div style={{ 'display': 'none' }} />
    )
  }

  useEffect(() => {
    setBoard(initBoard)
  });

  return (
    <Fragment>
      <div className="board">
        <Board 
          disableLaneDrag
          initialBoard={board}
          style={{ 'width': '100px', 'background': 'white', 'margin-top': '10px' }}
          renderLaneHeader={() => <LaneAdder />}
          onCardDragEnd={handleOnCardDragEnd}
          renderCard={({ laneId, title, description }, { dragging }) => (
            <Card laneId={laneId} dragging={dragging}>
              { description }

              AAA
            </Card>
          )}
        >
        {board}
        </Board>
      </div>
    </Fragment>
  )
}

export default Indicators
