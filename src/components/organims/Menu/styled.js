import styled from 'styled-components'

// eslint-disable-next-line import/prefer-default-export
export const Wrapper = styled.div`
  width: 70px;
  display: inline-block;
  background: #FFFFFF;
  height: 100%;
  vertical-align: top;
  padding-top: 40px;
`
