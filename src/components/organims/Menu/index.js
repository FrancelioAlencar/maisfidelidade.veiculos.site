import React, { useEffect, useState } from 'react'
import { Wrapper } from './styled'
import { Icon, ButtonMenu } from '@/components/atoms'
import instance from '@/config/client'
import { appMessages } from '@/helpers/utils'


const Menu = () => {
  const [tabs, setTabs] = useState([])

  useEffect(() => {
    instance.get('/v1/relacionamento/meurelacionamento?idempresa=0').then(e => {
      const items = [
        { text: 'Home', url: '/', id: 'home' },
        { text: 'Histórico', url: '/historico-de-pontos', id: 'star' },
        { text: 'Extrato de Benefícios', url: '/extrato-de-beneficios', id: 'graph' },
        { text: 'Simulador', url: '/simulador-de-pontos', id: 'check-input' },
        { text: 'Sua Loja / Indicadores', url: '/sua-loja', id: 'target' },
        { text: appMessages['how-it-works'].title, url: '/como-funciona', id: 'shape' }
      ]

      const userAllowed = e.data.Sucesso && e.data.Retorno.Elegibilidade && (e.data.Retorno.Elegibilidade.Elegivel || e.data.Retorno.Elegibilidade.Correntista) && e.data.Retorno.Elegibilidade.QuantidadeFraude <= 1 && e.data.Retorno.Elegibilidade.ElegivelFraude
      if (!userAllowed) {
        items.splice(1, 2)
      }

      setTabs(items)
    })
  }, [])

  return (
    <Wrapper>
      {tabs && tabs.length > 0 && tabs.map(item => {
        return (
          <ButtonMenu
            key={item.url}
            route={item.url}
            title={item.text}
            active={window && window.location.pathname === item.url && true}
            big
          >
            <Icon id={item.id} small />
          </ButtonMenu>
        )
      })}

    </Wrapper>
  )
}

export default Menu
