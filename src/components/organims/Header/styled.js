import styled from 'styled-components'

export const Wrapper = styled.div`
  height: 100px;
  background: #FFF;
  padding: 20px 40px 20px 80px;
`

export const WrapperImage = styled.div`
  position: relative;
  margin-top: 10px;

  svg {
    height: 50px;
    width: 315px;
  }
`
export const WrapperContent = styled.div`
  position: absolute;
  right: 0;
  top: 10px;
  text-align: right;
`