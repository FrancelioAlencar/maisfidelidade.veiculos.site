import React, { useEffect, useState } from 'react'
import moment from 'moment'
import instance from '@/config/client'
import { Icon, Label } from '@/components/atoms'
import { WrapperImage, Wrapper, WrapperContent } from './styled'

moment.locale('pt-br')

const Header = () => {
  const [myRating, setMyRating] = useState([])

  useEffect(() => {
    instance.get('/v1/classificacao/minhaClassificacao').then(e => {
      if (e.data.Sucesso) setMyRating(e.data.Retorno)
    })
  }, [])

  return (
    <Wrapper>
      <WrapperImage>
        <Icon id="logo-maisfidelidade" />
        <WrapperContent>
          <Label size={10} color={'#2e2c37'}>Última Atualização: </Label>
          <Label weight={'bold'} color={'#2e2c37'}>
            { myRating.DataReferencia ? moment(myRating.DataReferencia).format('DD/MM/YYYY') : '' } às {' '}
            { myRating.DataReferencia ? moment(myRating.DataReferencia).format('h:mm') : '00:00'}</Label>
        </WrapperContent>
      </WrapperImage>
    </Wrapper>
  )
}

export default Header
