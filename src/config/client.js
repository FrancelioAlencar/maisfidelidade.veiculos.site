import axios from 'axios'
import { authHeader } from '@/helpers/utils'
import { config } from './endpoints'

const instance = axios.create({
  baseURL: config.urlApi,
  timeout: 1000,
  headers: authHeader()
})

export default instance
