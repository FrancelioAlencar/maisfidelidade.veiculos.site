const configs = () => {
  let c = {}

  switch (process.env.NODE_ENV) {
  case 'development': {
    c.urlApiAutenticacao = 'https://hkapi.webmotors.com.br/plataformarevendedorcrm'
    c.urlApi = 'https://hkapi.webmotors.com.br/cockpit.maisfidelidade'
    break
  }
  case 'homologation': {
    c.urlApiAutenticacao = 'https://hkapi.webmotors.com.br/plataformarevendedorcrm'
    c.urlApi = 'https://hkapi.webmotors.com.br/cockpit.maisfidelidade'
    break
  }
  case 'blue': {
    c.urlApiAutenticacao = 'https://azulapicrmweb.webmotors.com.br'
    c.urlApi = 'https://azulapicrmweb.webmotors.com.br/cockpit.maisfidelidade'
    break
  }
  case 'production': {
    c.urlApiAutenticacao = 'https://apicrmweb.webmotors.com.br'
    c.urlApi = 'https://apicrmweb.webmotors.com.br/cockpit.maisfidelidade'
    break
  }
  default:
    return false
  }

  c.urlApiLogin = `${c.urlApiAutenticacao}/v3/login/maisfidelidade`
  c.urlApiToken = `${c.urlApiAutenticacao}/token`
  c.urlApiInfoUser = `${c.urlApiAutenticacao}/v3/acesso`
  c.urlApiConfirmarEmail = `${c.urlApiAutenticacao}/v3/confirmarEmail/maisfidelidade`
  c.urlApiEsqueciSenha = `${c.urlApiAutenticacao}/v3/esquecisenha/maisfidelidade`
  c.urlApiResetarSenha = `${c.urlApiAutenticacao}/v3/resetarsenha/maisfidelidade`
  c.urlApiMinhaEvolucao = `${c.urlApi}/v1/relatorios/minhaevolucao`
  c.urlApiHistoricoPontos = `${c.urlApi}/v1/relacionamento/meurelacionamentomensal`
  c.urlApiDadosRevenda = `${c.urlApi}/v1/revenda/dadosRevenda`
  return c
}

const ApiLogin = (() => {
  const url = {
    development: 'https://hk.webmotors.com.br/api',
    homologation: 'https://hk.webmotors.com.br/api',
    blue: 'https://azulcockpit.webmotors.com.br/api',
    production: 'https://cockpit.webmotors.com.br/api'
  }
  return url[process.env.NODE_ENV]
})()

const ApiCockpit = (() => {
  const url = {
    development: 'https://localhost:44351/api',
    homologation: 'https://apihcockpit.webmotors.com.br/api',
    blue: 'https://apibetacockpit.webmotors.com.br/api',
    production: 'https://apibetacockpit.webmotors.com.br/api'
  }
  return url[process.env.NODE_ENV]
})()

const ApiSisense = (() => {
  const url = {
    development: 'https://sisense.webmotors.com.br/api/',
    homologation: 'https://sisense.webmotors.com.br/api/',
    blue: 'https://sisense.webmotors.com.br/api/',
    production: 'https://sisense.webmotors.com.br/api/'
  }
  return url[process.env.NODE_ENV]
})()

const UrlCockpit = (() => {
  const url = {
    development: 'http://local.webmotors.com.br:8080',
    homologation: 'https://hcockpit.webmotors.com.br',
    blue: 'https://azulcockpit.webmotors.com.br',
    production: 'https://cockpit.webmotors.com.br'
  }
  return url[process.env.NODE_ENV]
})()

const UrlCRM = (() => {
  const url = {
    development: 'http://local.webmotors.com.br:81/plataformarevendedor',
    homologation: 'https://hkcrm.webmotors.com.br',
    blue: 'https://azulcrm.webmotors.com.br',
    production: 'https://crm.webmotors.com.br'
  }
  return url[process.env.NODE_ENV]
})()

const UrlEstoque = (() => {
  const url = {
    development: 'http://local.webmotors.com.br:8080',
    homologation: 'https://hestoque.webmotors.com.br',
    blue: 'https://azulestoque.webmotors.com.br',
    production: 'https://estoque.webmotors.com.br'
  }
  return url[process.env.NODE_ENV]
})()

const config = configs()
export {
  config,
  ApiLogin,
  ApiCockpit,
  ApiSisense,
  UrlCockpit,
  UrlCRM,
  UrlEstoque
}

export default config
