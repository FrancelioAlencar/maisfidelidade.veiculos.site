import 'babel-polyfill'
import React from 'react'
import { BrowserRouter as Router } from 'react-router-dom'
import { AppContainer } from 'react-hot-loader'
import { Provider } from 'react-redux'
import ReactDOM from 'react-dom'
import Main from 'webmotors-react-pj/main'
import Routes from './routes'
import { Menu, Header } from '@/components/organims'
import { GlobalStyles } from '@/helpers/styles'
import { Wrapper, WrapperContent } from '@/helpers/styles/BaseWrapper'
import store from '@/redux/store'

ReactDOM.render(
  <AppContainer>
    <Provider store={store}>
      <Router>
        <Main>
          <GlobalStyles />
          <Wrapper>
            <Header />
            <Menu />
            <WrapperContent>
              <Routes />
            </WrapperContent>
          </Wrapper>
        </Main>
      </Router>
    </Provider>
  </AppContainer>,
  document.getElementById('root'),
)
