import { combineReducers } from 'redux'
import platinum from './platinum'

export default combineReducers({
  platinum
})
