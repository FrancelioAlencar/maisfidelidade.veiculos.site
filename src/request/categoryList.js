class CategoryList {
  static async getAllBenefits(count) {
    return Array.from({ length: count }, (v, k) => k).map(k => ({
      id: `item-${k}`,
      content: `item ${k}`,
      percentage: (k + 2)
    }))
  }
}

export default CategoryList
