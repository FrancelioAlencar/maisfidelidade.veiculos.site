import instance from '@/config/client'

class Indicators {
  static async getIndicators() {
    let status = {}
    await instance.get('/v1/indicador/indicadoresdequalidade').then(e => {
      if (e.data.Sucesso) status = { error: false, data: e.data.Retorno }
    }).catch(e => {
      status = { error: true, message: e.message }
    })

    return status
  }
}

export default Indicators
