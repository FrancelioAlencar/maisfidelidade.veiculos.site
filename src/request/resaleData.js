import instance from '@/config/client'

class ResaleData {
  static async getData() {
    let status = {}
    await instance.get('/v1/revenda/dadosRevenda').then(e => {
      if (e.data.Sucesso) status = { error: false, data: e.data.Retorno }
    }).catch(e => {
      status = { error: true, message: e.message }
    })

    return status
  }
}

export default ResaleData
