import instance from '@/config/client'

class MyRating {
  static async getRating() {
    let status = {}
    await instance.get('/v1/classificacao/minhaClassificacao').then(e => {
      if (e.data.Sucesso) status = { error: false, data: e.data.Retorno }
    }).catch(e => {
      status = { error: true, message: e.message }
    })

    return status
  }
}

export default MyRating
