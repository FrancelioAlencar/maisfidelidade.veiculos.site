import React from 'react'
import { Route, Switch } from 'react-router'
import Dashboard from '@/components/pages/Dashboard'
import HowItWorks from '@/components/pages/HowItWorks'
import PointsSimulator from '@/components/pages/PointsSimulator'

const routes = [
  {
    id: 0,
    path: '/',
    exact: true,
    component: Dashboard
  },
  {
    id: 1,
    path: '/como-funciona',
    component: HowItWorks
  },
  {
    id: 2,
    path: '/simulador-de-pontos',
    component: PointsSimulator
  },
]

export default (props) => {
  const renderMergedProps = (component, ...rest) => {
    const finalProps = Object.assign({}, ...rest)
    return React.createElement(component, finalProps)
  }

  const PropsRoute = ({ component, ...rest }) => (
    <Route {...rest} render={routeProps => renderMergedProps(component, routeProps, rest)} />
  )

  return (
    <Switch>
      {routes.map(route => (
        <PropsRoute key={route.id} {...route} {...props} />
      ))}
    </Switch>
  )
}
