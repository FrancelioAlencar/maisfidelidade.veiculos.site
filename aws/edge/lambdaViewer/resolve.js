const CONSTANTS = require('./constants');
const s3Request = require('./s3_request');

exports.balancers = (event) => {
    const { request } = event.Records[0].cf;
    const balancer = CONSTANTS.DISTRIBUTION_ID.find(
        x => event.Records[0].cf.config.distributionId === x.id,
    );
    return s3Request.get(request, balancer.site);
};
