module.exports = Object.freeze({
    DISTRIBUTION_ID: [
        {
            id: 'E1LZ0TDI2UG5S6',
            env: 'homologation',
            name: 'hcockpit.webmotors.com.br',
            site: {
                load_balancer: 'webmotors-cockpit.s3.amazonaws.com',
                path: '/site/homologation/source/cockpit',
            },
        },
        {
            id: 'E1AZZAESI1EBW',
            env: 'blue',
            name: 'azulcockpit.webmotors.com.br',
            site: {
                load_balancer: 'webmotors-cockpit.s3.amazonaws.com',
                path: '/site/blue/source/cockpit',
            },
        },
        {
            id: 'E3UC58HQVVOWIE',
            env: 'production',
            name: 'cockpit.webmotors.com.br',
            site: {
                load_balancer: 'webmotors-cockpit.s3.amazonaws.com',
                path: '/site/production/source/cockpit',
            },
        },
    ],
});
