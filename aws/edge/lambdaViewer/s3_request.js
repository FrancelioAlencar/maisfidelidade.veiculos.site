exports.get = (request, config) => {
    request.origin = {
        s3: {
            authMethod: 'origin-access-identity',
            customHeaders: {},
            domainName: config.load_balancer,
            path: config.path,
            region: 'us-east-1',
        },
    };

    if (request.uri.indexOf('apple-app-site-association') !== -1) {
        request.uri = '/data/apple-app-site-association.json';
    } else if (request.uri.indexOf('/assets/') === -1
        && request.uri.indexOf('/img/') === -1
        && request.uri.indexOf('.css') === -1
        && request.uri.indexOf('.js') === -1
        && request.uri.indexOf('.ico') === -1
    ) {
        request.uri = '/index.html';
    }
    return request;
};
