'use strict';

const resolve = require('./resolve.js');

exports.handler = (event, context, callback) => {
    const request = resolve.balancers(event);
    console.log(JSON.stringify(request));
    callback(null, request);
};
